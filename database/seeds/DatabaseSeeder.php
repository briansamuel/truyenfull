<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //  for($i = 0; $i < 100; $i++) {
              
        //     //   $this->call(PostsTableSeeder::class);
        //     //   $this->call(AgentsTableSeeder::class);
        //     // //   $this->call(CategoriesTableSeeder::class);
        //     //   $this->call(GuestsTableSeeder::class);
        //  }
        $this->call(UsersTableSeeder::class);
        //  for($i = 0; $i < 100; $i++) {
        //      $this->call(LogsUserTableSeeder::class);
        //  }
        //  for($i = 0; $i < 15; $i++) {
        //      $this->call(UserGroupsTableSeeder::class);
        //  }
    }
        
}
