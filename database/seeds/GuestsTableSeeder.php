<?php

use Illuminate\Database\Seeder;

class GuestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guests')->insert([
            'username' => Str::random(10),
            'password' => Str::random(40),
            'email' =>  Str::random(40).'@gmail.com',
            'full_name' =>  Str::random(40),
            'active_code' => Str::random(16),
            'group_id' => '',
            'guest_avatar' => Str::random(16),
            'guest_address' => Str::random(36),
            'guest_phone' => mt_rand(),
            'guest_birthday' => date("Y-m-d H:i:s"),
            'status' =>  'active',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
