<?php

return [
  'web' => [
    'domain' => env('DOMAIN_GUEST'),
    'admin' => env('DOMAIN_ADMIN'),
    'agent' => env('DOMAIN_AGENT'),
  ],
  'api' => [
    'domain' => env('DOMAIN_GUEST'),
    'admin' => env('DOMAIN_ADMIN'),
    'agent' => env('DOMAIN_AGENT'),
  ]
];
