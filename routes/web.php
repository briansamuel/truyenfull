<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::domain('senhos.local')->group(function () {

if (!defined('FM_USE_ACCESS_KEYS')) {
    define('FM_USE_ACCESS_KEYS', true); // TRUE or FALSE
}
Route::domain(Config::get('domain.web.domain'))->group(function () {
    Route::namespace('FrontSite')->group(function () {
        Route::get('/', ['uses' => 'HomeController@index'])->name('home');
        Route::get('/ajax.php', ['uses' => 'StoryController@ajaxStory'])->name('ajax');
        Route::get('/ajax.html', ['uses' => 'StoryController@ajaxStory'])->name('ajax');
        Route::post('/ajax.php', ['uses' => 'StoryController@ajaxStory'])->name('ajax.Post');
        // Thể loại
        Route::get('/the-loai/{genres_slug}', ['uses' => 'GenresController@index'])->name('geresStories');
        Route::get('/the-loai/{genres_slug}/trang-{page}/', ['uses' => 'GenresController@index'])->name('geresStories.page');
        Route::get('/the-loai/{genres_slug}/{status}/', ['uses' => 'GenresController@indexStatus'])->name('geresStatusStories');
        Route::get('/the-loai/{genres_slug}/{status}/trang-{page}/', ['uses' => 'GenresController@indexStatus'])->name('geresStatusStories.page');    
        Route::get('/cover/files/{thumbnail_name}.{extension}', ['uses' => 'GenresController@cover'])->name('geresCover'); 
        Route::get('/cover/files/{year}/{month}/{thumbnail_name}.{extension}', ['uses' => 'GenresController@coverYearMonth'])->name('coverYearMonth'); 
        
        // 
        Route::get('/danh-sach/{slug}/', ['uses' => 'StoryController@list'])->name('listStories');
        Route::get('/danh-sach/{slug}/trang-{page}', ['uses' => 'StoryController@list'])->name('listStories.Page');
        // Trang tìm kiếm
        Route::get('/tim-kiem', ['uses' => 'StoryController@search'])->name('story.search');
        
        // Trang truyện chi tiết
        Route::get('/{story_slug}', ['uses' => 'StoryController@index'])->name('storyDetail');
        Route::get('/{story_slug}/trang-{page}/', ['uses' => 'StoryController@index'])->name('storyDetail');
        // Chi tiết Chương truyện
        Route::get('/{story_slug}/{chapter_slug}', ['uses' => 'ChapterController@index'])->name('chapterDetail');
        
       

    });
});

Route::domain(Config::get('domain.web.admin'))->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        });
        // login
        Route::get('/login', ['uses' => 'LoginController@login'])->name('login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('resetPassword');
        Route::get('/active-user', ['uses' => 'LoginController@activeUser'])->name('activeUser');
        Route::get('/active-agent', ['uses' => 'LoginController@activeAgent'])->name('activeAgent');
        Route::get('/active-guest', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');
    });

    Route::middleware(['auth'])->group(function () {
        
        Route::get('/', ['uses' => 'Admin\DashboardController@index'])->name('dash-board');
        Route::get('/welcome', ['uses' => 'Admin\DashboardController@index'])->name('welcome');

        Route::namespace('Admin')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('profile.changePasswordAction');

            Route::namespace('CMS')->group(function () {

                // Page
                Route::get('/page', ['uses' => 'PageController@index'])->name('page.list');
                Route::get('/page/add', ['uses' => 'PageController@add'])->name('page.add');
                Route::post('/page/add', ['uses' => 'PageController@add'])->name('page.add.action');
                Route::get('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit');
                Route::post('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit.action');
                Route::post('/page/edit', ['uses' => 'PageController@editManyAction'])->name('page.edit.many.action');
                Route::get('/page/delete/{service_id}', ['uses' => 'PageController@delete'])->name('page.delete');
                Route::post('/page/delete', ['uses' => 'PageController@deletemany'])->name('page.delete.many');
                Route::post('/page/save', ['uses' => 'PageController@save'])->name('page.save');
                Route::get('/page/ajax/get-list', ['uses' => 'PageController@ajaxGetList'])->name('page.ajax.getList');

                // News
                Route::get('/news', ['uses' => 'NewsController@index'])->name('news.list');
                Route::get('/news/add', ['uses' => 'NewsController@add'])->name('news.add');
                Route::get('/news/edit/{news_id}', ['uses' => 'NewsController@add'])->name('news.edit');
                Route::post('/news/edit', ['uses' => 'NewsController@editManyAction'])->name('news.edit.many.action');
                Route::get('/news/delete/{news_id}', ['uses' => 'NewsController@delete'])->name('news.delete');
                Route::post('/news/delete', ['uses' => 'NewsController@deletemany'])->name('news.delete.many');
                Route::post('/news/save', ['uses' => 'NewsController@save'])->name('news.save');
                Route::get('/news/ajax/get-list', ['uses' => 'NewsController@ajaxGetList'])->name('news.ajax.getList');

                // Story
                Route::get('/story', ['uses' => 'StoryController@index'])->name('story.list');
                Route::get('/story/add', ['uses' => 'StoryController@add'])->name('story.add');
                Route::post('/story/add', ['uses' => 'StoryController@addAction'])->name('story.add.action');
                Route::get('/story/edit/{story_id}', ['uses' => 'StoryController@edit'])->name('story.edit');
                Route::post('/story/edit/{story_id}', ['uses' => 'StoryController@editAction'])->name('story.edit.action');
                Route::post('/story/edit', ['uses' => 'StoryController@editManyAction'])->name('story.edit.many.action');
                Route::get('/story/delete/{news_id}', ['uses' => 'StoryController@delete'])->name('story.delete');
                Route::post('/story/delete', ['uses' => 'StoryController@deletemany'])->name('story.delete.many');
                Route::get('/story/ajax/get-list', ['uses' => 'StoryController@ajaxGetList'])->name('story.ajax.getList');
                
                // Chapter
                Route::get('/chapter', ['uses' => 'ChapterController@index'])->name('chapter.list');
                Route::get('/chapter/add', ['uses' => 'ChapterController@add'])->name('chapter.add');
                Route::post('/chapter/add', ['uses' => 'ChapterController@addAction'])->name('chapter.add.action');
                Route::get('/chapter/edit/{chapter_id}', ['uses' => 'ChapterController@edit'])->name('chapter.edit');
                Route::post('/chapter/edit/{chapter_id}', ['uses' => 'ChapterController@editAction'])->name('chapter.edit.action');
                Route::post('/chapter/edit', ['uses' => 'ChapterController@editManyAction'])->name('chapter.edit.many.action');
                Route::get('/chapter/delete/{news_id}', ['uses' => 'StoryController@delete'])->name('chapter.delete');
                Route::post('/chapter/delete', ['uses' => 'ChapterController@deletemany'])->name('chapter.delete.many');
                Route::get('/chapter/ajax/get-list', ['uses' => 'ChapterController@ajaxGetList'])->name('chapter.ajax.getList');

                // Comment
                Route::get('/comment', ['uses' => 'CommentController@index'])->name('comment.list');
                Route::get('/comment/add', ['uses' => 'CommentController@add'])->name('comment.add');
                Route::post('/comment/add', ['uses' => 'CommentController@addAction'])->name('comment.add.action');
                Route::get('/comment/edit/{comment_id}', ['uses' => 'CommentController@edit'])->name('comment.edit');
                Route::post('/comment/edit/{comment_id}', ['uses' => 'CommentController@editAction'])->name('comment.edit.action');
                Route::post('/comment/edit', ['uses' => 'CommentController@editManyAction'])->name('comment.edit.many.action');
                Route::get('/comment/delete/{comment_id}', ['uses' => 'CommentController@delete'])->name('comment.delete');
                Route::post('/comment/delete', ['uses' => 'CommentController@deletemany'])->name('comment.delete.many');
                Route::get('/comment/ajax/get-list', ['uses' => 'CommentController@ajaxGetList'])->name('comment.ajax.getList');

                // Category
                Route::get('/category', ['uses' => 'CategoryController@index'])->name('category.list');
                Route::post('/category/save', ['uses' => 'CategoryController@save'])->name('category.save');
                Route::get('/category/edit/{category_id}', ['uses' => 'CategoryController@edit'])->name('category.edit');
                Route::post('/category/edit', ['uses' => 'CategoryController@editManyAction'])->name('category.edit.many.action');
                Route::post('/category/edit/{category_id}', ['uses' => 'CategoryController@editAction'])->name('category.edit.action');
                Route::get('/category/delete/{category_id}', ['uses' => 'CategoryController@delete'])->name('category.delete');
                Route::post('/category/delete', ['uses' => 'CategoryController@deleteMany'])->name('category.deleteMany');
                Route::get('/category/ajax/get-list', ['uses' => 'CategoryController@ajaxGetList'])->name('category.ajax.getList');
                // User
                Route::get('/user', ['uses' => 'UsersController@index'])->name('user.list');
                Route::get('/user/add', ['uses' => 'UsersController@add'])->name('user.add');
                Route::post('/user/add', ['uses' => 'UsersController@addAction'])->name('user.add.action');
                Route::get('/user/detail/{user_id}', ['uses' => 'UsersController@detail'])->name('user.detail');
                Route::get('/user/edit/{user_id}', ['uses' => 'UsersController@edit'])->name('user.edit');
                Route::post('/user/edit', ['uses' => 'UsersController@editManyAction'])->name('user.edit.many.action');
                Route::post('/user/edit/{user_id}', ['uses' => 'UsersController@editAction'])->name('user.edit.action');
                Route::get('/user/delete', ['uses' => 'UsersController@deleteMany'])->name('user.delete.many');
                Route::get('/user/delete/{user_id}', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::post('/user/delete', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::get('/user/ajax/get-list', ['uses' => 'UsersController@ajaxGetList'])->name('user.ajax.getList');


                // Agent
                Route::get('/user-group', ['uses' => 'UserGroupController@index'])->name('user_group.list');
                Route::get('/user-group/add', ['uses' => 'UserGroupController@add'])->name('user_group.add');
                Route::post('/user-group/add', ['uses' => 'UserGroupController@addAction'])->name('user_group.add.action');
                Route::get('/user-group/detail/{guest_id}', ['uses' => 'UserGroupController@detail'])->name('user_group.detail');
                Route::get('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@edit'])->name('user_group.edit');
                Route::post('/user-group/edit', ['uses' => 'UserGroupController@editManyAction'])->name('user_group.edit.many.action');
                Route::post('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@editAction'])->name('user_group.edit.action');
                Route::get('/user-group/delete', ['uses' => 'UserGroupController@deleteMany'])->name('user_group.delete.many');
                Route::get('/user-group/delete/{guest_id}', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::post('/user-group/delete', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::get('/user-group/ajax/get-list', ['uses' => 'UserGroupController@ajaxGetList'])->name('user_group.ajax.getList');

                // Logs User
                Route::get('/logs-user', ['uses' => 'LogsUserController@index'])->name('logs_user.list');
                Route::get('/logs-user/detail/{id}', ['uses' => 'LogsUserController@detail'])->name('logs_user.detail');
                Route::get('/logs-user/ajax/get-list', ['uses' => 'LogsUserController@ajaxGetList'])->name('logs_user.ajax.getList');

                // Menu
                Route::get('/menus', ['uses' => 'MenuController@index'])->name('menu.index');

                // Custom Css
                Route::get('/theme-option', ['uses' => 'ThemeOptionsController@option'])->name('theme_option.index');
                Route::post('/theme-option', ['uses' => 'ThemeOptionsController@optionAction'])->name('theme_option.action');

                // Custom Css
                Route::get('/custom-css', ['uses' => 'CustomCssController@index'])->name('custom_css.index');
                Route::post('/custom-css', ['uses' => 'CustomCssController@editAction'])->name('custom_css.editAction');

                // Contacts
                Route::get('/contact', ['uses' => 'ContactController@index'])->name('contact.index');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::get('/contact/edit/{id}', ['uses' => 'ContactController@edit'])->name('contact.edit');
                Route::post('/contact/edit', ['uses' => 'ContactController@editManyAction'])->name('contact.edit.many.action');
                Route::post('/contact/edit/{id}', ['uses' => 'ContactController@editAction'])->name('contact.edit.action');
                Route::get('/contact/delete', ['uses' => 'ContactController@deleteMany'])->name('contact.delete.many');
                Route::get('/contact/delete/{id}', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::post('/contact/delete', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::post('/contact/{id}/reply', ['uses' => 'ContactController@replyAction'])->name('contact.reply.action');

                // Setting
                Route::get('/settings-general', ['uses' => 'SettingController@general'])->name('setting.general');
                Route::post('/settings-general', ['uses' => 'SettingController@generalAction'])->name('setting.general.action');
                Route::post('/settings-login-social', ['uses' => 'SettingController@loginSocialAction'])->name('setting.login_social.action');
                Route::get('/settings-email', ['uses' => 'SettingController@email'])->name('setting.email');
                Route::get('/settings-social-login', ['uses' => 'SettingController@loginSocial'])->name('setting.login_social');
                Route::get('/settings-notification', ['uses' => 'SettingController@notification'])->name('setting.login_social');
                Route::post('/settings-notification', ['uses' => 'SettingController@notificationAction'])->name('setting.login_social');
                
                // Gallery
                Route::get('/gallery', ['uses' => 'GalleryController@index'])->name('gallery.index');
            });

            
        });
        Route::namespace('General')->group(function () {
            Route::post('/upload-image', ['uses' => 'UpLoadImageController@uploadImage'])->name('uploadImage');
            
            //Route::post('/filemanager/upload.php', ['uses' => 'UpLoadImageController@uploadImage'])->name('fileManagerUpload');
        });
        Route::namespace('Crawl')->group(function () {
            Route::get('/test-demo', ['uses' => 'CrawlTruyenCVController@index'])->name('test-demo');
            Route::get('/list-page', ['uses' => 'CrawlTruyenCVController@listPage'])->name('listPage');
            Route::get('/crawl-data', ['uses' => 'CrawlTruyenCVController@crawl'])->name('crawlData2');
            Route::get('/crawl-chapter', ['uses' => 'CrawlTruyenCVController@crawlChapter'])->name('crawlChapter');
            Route::get('/crawl/ajax/list-story', ['uses' => 'CrawlTruyenCVController@ajaxListStory'])->name('listStory');
            Route::get('/crawl/ajax/list-chapter', ['uses' => 'CrawlTruyenCVController@ajaxListChapter'])->name('listChapter');
            Route::post('/crawl/ajax/list-chapter', ['uses' => 'CrawlTruyenCVController@ajaxListChapter2'])->name('listChapter');
            
            Route::post('/crawl/ajax/add-chapter', ['uses' => 'CrawlTruyenCVController@ajaxAddChapter'])->name('addChapter');
            Route::get('/crawl/ajax/update-crawl-logs', ['uses' => 'CrawlTruyenCVController@ajaxUpdateCrawlLogs'])->name('updateCrawlLogs');
        });
        
    });

});

Route::domain(Config::get('domain.web.agent'))->group(function () {
    Route::namespace('Agent')->group(function () {
        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        });
        // login
        Route::get('/login', ['uses' => 'LoginController@login'])->name('agent.login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('agent.loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('agent.logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('agent.resetPassword');
    });

    Route::middleware(['agent_auth'])->group(function () {

        Route::get('/', ['uses' => 'Agent\DashboardController@index'])->name('agent.dash-board');
        Route::get('/welcome', ['uses' => 'Agent\DashboardController@index'])->name('agent.welcome');

        Route::namespace('Agent')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('agent.profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('agent.profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('agent.profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('agent.profile.changePasswordAction');

            Route::namespace('CMS')->group(function () {
                // Booking
                Route::get('/booking', ['uses' => 'BookingController@index'])->name('agent.booking.list');
                Route::get('/booking/add', ['uses' => 'BookingController@add'])->name('agent.booking.add');
                Route::post('/booking/add', ['uses' => 'BookingController@addAction'])->name('agent.booking.add.action');
                Route::get('/booking/edit/{booking_id}', ['uses' => 'BookingController@edit'])->name('agent.booking.edit');
                Route::post('/booking/edit/{booking_id}', ['uses' => 'BookingController@editAction'])->name('agent.booking.edit.action');
                Route::post('/booking/edit', ['uses' => 'BookingController@editManyAction'])->name('agent.booking.edit.many.action');
                Route::get('/booking/delete/{news_id}', ['uses' => 'BookingController@delete'])->name('agent.booking.delete');
                Route::post('/booking/delete', ['uses' => 'BookingController@deletemany'])->name('agent.booking.delete.many');
                Route::get('/booking/ajax/get-list', ['uses' => 'BookingController@ajaxGetList'])->name('agent.booking.ajax.getList');
                Route::get('/booking/ajax/get-district', ['uses' => 'BookingController@ajaxGetDistrict'])->name('agent.booking.ajax.getDistrict');
                Route::get('/booking/ajax/get-ward', ['uses' => 'BookingController@ajaxGetWard'])->name('agent.booking.ajax.getWard');
            });
        });

    });

});

