@extends('admin.index')
@section('page-header', 'Truyện')
@section('page-sub_header', 'Danh sách truyện')
@section('style')

@endsection
@section('content')

<div class="row">

    <div class="kt-portlet" data-ktportlet="true">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    CRAWL DATA
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('news.add') }}" class="btn btn-brand kt-btn">
                        <i class="flaticon2-plus"></i>
                        CRAWL DATA TRUYỆN
                    </a>

                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-12">
                <div class="kt-portlet">

                    <!--begin::Form-->
                    <div class="kt-form ">
                        <div class="kt-portlet__body">

                            
                             <!--begin: Search Form -->
                            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                <div class="row align-items-center">
                                    <div class="col-xl-12 order-2 order-xl-1">
                                        <div class="row align-items-center">
                                            <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                                                <label>Từ khóa:</label>
                                                <div class="kt-input-icon kt-input-icon--left">
                                                    <input type="text" class="form-control" placeholder="Vui lòng nhập từ khóa..." id="story_title">
                                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                        <span><i class="la la-search"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                                                <div class="form__group kt-form__group--inline">
                                                    <label>Tình trạng:</label>
                                                    <select class="form-control bootstrap-select" id="kt_form_status1">
                                                        <option value="">Tất cả</option>
                                                        <option value="draft">Nháp</option>
                                                        <option value="coming">Đang ra</option>
                                                        <option value="pending">Chờ duyệt</option>
                                                        <option value="full">Đã hoàn thành</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                                                <div class="form__group kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label>Danh mục:</label>
                                                    </div>
                                                    <select class="form-control bootstrap-select" id="kt_form_type1">
                                                        <option value="">All</option>
                                                        @foreach($sourceTruyen as $key => $item)
                                                        <option value="{{ $item }}">{{ $item }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 kt-margin-b-20-tablet-and-mobile" id="kt_form_daterange1">
                                                <div class="form__group kt-form__group--inline">
                                                    <div class="kt-form__label">
                                                        <label>Theo ngày tạo:</label>
                                                    </div>
                                                    <input type='text' class="form-control" id="kt_daterangepicker_1" readonly placeholder="Chọn thời gian" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                                        <a href="#" class="btn btn-default kt-hidden">
                                            <i class="la la-cart-plus"></i> New Order
                                        </a>
                                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Search Form -->
                            <!--begin: Selected Rows Group Action Form -->
                            <div class="kt-form kt-form--label-align-right kt-margin-t-20 collapse" id="kt_datatable_group_action_form">
                                <div class="row align-items-center">
                                    <div class="col-xl-12">
                                        <div class="kt-form__group kt-form__group--inline">
                                            <div class="kt-form__label kt-form__label-no-wrap">
                                                <label class="kt-font-bold kt-font-danger-">Chọn
                                                    <span id="kt_datatable_selected_number1">0</span> dòng:</label>
                                                {{csrf_field()}}
                                            </div>
                                            <div class="kt-form__control">
                                                <div class="btn-toolbar">
                                                    <div class="dropdown">
                                                        <button id="btn-clear-console" type="button" class="btn btn-primary disabled " disabled><i class="la la-desktop"></i> Xóa Màn Hình</button>
                                                        <button id="btn-stop-crawl" type="button" class="btn btn-primary disabled" disabled><i class="la la-stop"></i> Tạm dừng</button>
                                                        <button id="btn-restart" type="button" class="btn btn-primary disabled" disabled><i class="la la-share-alt-square"></i> Tiếp tục lấy</button>
                                                        <button id="btn-crawl-chapter" type="button" class="btn btn-primary"><i class="la la-hand-o-right"></i> Lấy chương</button>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Selected Rows Group Action Form -->
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                           
                            <!--begin: Datatable -->
                            <div class="kt-datatable" id="server_story_crawler_selection"></div>

                            <!--end: Datatable -->

                            
                        </div>
                        <div  class="kt-portlet__body">

                            <div class="form-group row">
                                <label for="story_description" class="col-12 col-form-label"><strong>Trang hiện tại :</strong> <span class="current-page kt-font-danger kt-font-italic"></span></label>
                                <label for="story_description" class="col-12 col-form-label"><strong>Kết quả :</strong> <span class="result-message"></span></label>
                                <div class="col-12 ">
                                    <label class="col-12 col-form-label"><strong>Danh sách chương :</strong></label>
                                    <div id="list_story" class="kt-section">

                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated  bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @endsection
    @section('script')
    <!--begin::Page Vendors(used by this page) -->


    <!--end::Page Vendors -->
    <script src="admin/js/pages/crawlers/chapter.js" type="text/javascript"></script>
    <script src="admin/js/pages/crawlers/index.js" type="text/javascript"></script>


    <!-- <script src="assets/js/pages/crud/metronic-datatable/advanced/record-selection.js" type="text/javascript"></script> -->

    @endsection