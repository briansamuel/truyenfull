@foreach ($hot_stories as $key => $story)


    <div class="item top-{{ ++$key }}" itemscope itemtype="https://schema.org/Book">
        <a href="{{ $story->story_slug }}" itemprop="url">
            @if ($story->story_status == 'full')
                <span class="full-label"></span>
            @endif
            <img src="{{ $story->story_thumbnail }}" alt="{{ $story->story_title }}" class="img-responsive item-img"
                itemprop="image">
            <div class="title">
                <h3 itemprop="name">{{ $story->story_title }}</h3>
            </div>
        </a>
    </div>
@endforeach
