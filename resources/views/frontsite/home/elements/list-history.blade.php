<div class="list list-truyen list-history col-xs-12">
    <div class="title-list">
        <h2>Truyện đang đọc</h2>
    </div>
    @foreach ($stories as $story)
        @php
            $story = (object) $story;
        @endphp
        <div class="row">
            <div class="col-md-5 col-lg-7"><span class="glyphicon glyphicon-chevron-right"></span>
                <h3 itemprop="name"><a href="{{ url($story->story_slug) }}" title="{{ $story->story_title }}">{{ $story->story_title }}</a></h3>
            </div>
            <div class="col-md-7 col-lg-5 text-info"><a
                    href="{{ url($story->story_slug . '/' . $story->chapter_slug) }}">Đọc tiếp
                    C{{ $story->chapter_order }}</a></div>
        </div>
    @endforeach

</div>
