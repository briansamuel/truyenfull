@foreach ($lastest_stories as $key => $story)
    <div class="row" itemscope itemtype="https://schema.org/Book">
        <div class="col-xs-9 col-sm-6 col-md-5 col-title"><span class="glyphicon glyphicon-chevron-right"></span>
            <h3 itemprop="name"><a href="{{ $story->story_slug }}" title="{{ $story->story_title }}"
                    itemprop="url">{{ $story->story_title }}</a></h3>
            @if($story->story_status == 'full')
            <span class="label-title label-full"></span>
            @endif
            @if($story->story_feature == 1)
            <span class="label-title label-hot"></span>
            @endif
        </div>
        <div class="hidden-xs col-sm-3 col-md-3 col-cat text-888">{!! $story->story_genres !!}</div>
        <div class="col-xs-3 col-sm-3 col-md-2 col-chap text-info"><a
                href="{{ $story->story_slug . '/' . $story->chapter_slug }}/"
                title="{{ $story->story_title }} - {{ $story->chapter_title }}"><span
                    class="chapter-text">{{ $story->chapter_order }}</span></a></div>
        <div class="hidden-xs hidden-sm col-md-2 col-time text-888">

            {{ Carbon\Carbon::parse($story->updated_at)->diffForHumans() }}
        </div>
    </div>
@endforeach
