<div class="visible-md-block visible-lg-block col-md-4 text-center col-truyen-side">
    <div class="hide" id="history-holder-side"></div>
    <div class="list list-truyen list-cat col-xs-12">
        <div class="title-list">
            <h4>Thể loại truyện</h4>
        </div>
        <div class="row">
            @foreach ($categories as $key => $category)

                <div class="col-xs-6"><a href="the-loai/{{ $category->category_slug }}/"
                        title="{{ $category->category_seo_title }}">{{ $category->category_name }}</a></div>
            @endforeach


        </div>
    </div>
</div>
