<div class="container" id="truyen-slide">
    <div class="list list-thumbnail col-xs-12">
        <div class="title-list">
            <h2><a href="{{ route('listStories', ['slug' => 'truyen-full']) }}/" title="Truyện full">Truyện đã hoàn thành</a></h2><a href="danh-sach/truyen-full/" title="Truyện full"><span class="glyphicon glyphicon-menu-right"></span></a>
        </div>


        <div class="row">
            @foreach($full_stories as $key => $story)
            <div class="col-xs-4 col-sm-3 col-md-2">
                <a href="{{ $story->story_slug }}" title="{{ $story->story_title }}">
                    <div class="lazyimg" data-desk-image="{{ $story->story_thumbnail ? $story->story_thumbnail : '/img/default-thumbnail.jpg' }}" data-alt="{{ $story->story_title }}"></div>
                    <div class="caption">
                        <h3>{{ $story->story_title }}</h3><small class="btn-xs label-primary">Full - {{ $story->total_chapter }} chương</small>
                    </div>
                </a>
            </div>
            @endforeach
        </div>

    </div>
</div>