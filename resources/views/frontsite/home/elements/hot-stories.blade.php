<div class="container" id="intro-index">
    <div class="title-list">
        <h2><a href="{{ route('listStories', ['slug' => 'truyen-hot']) }}/" title="Truyện hot">Truyện hot</a></h2><a href="{{ route('listStories', ['slug' => 'truyen-hot']) }}/" title="Truyện hot"><span class="glyphicon glyphicon-fire"></span></a>
        <select id="hot-select" class="form-control new-select" aria-label="Chọn thể loại">
            <option value="all">Tất cả</option>
            @foreach ($categories as $key => $category)
                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="index-intro">
        @foreach($hot_stories as $key => $story)
        
        
        <div class="item top-{{ ++$key }}" itemscope itemtype="https://schema.org/Book">
            <a href="{{ $story->story_slug }}" itemprop="url">
                @if($story->story_status == 'full')
                <span class="full-label"></span>
                @endif
                <img src="img/pixel-cover.gif" lazysrc="{{ $story->story_thumbnail }}" alt="{{ $story->story_title }}" class="img-responsive item-img" itemprop="image">
                <div class="title">
                    <h3 itemprop="name">{{ $story->story_title }}</h3>
                </div>
            </a>
        </div>
        @endforeach
        
    </div>
</div>