@extends('frontsite.index')
@section('title', 'Đọc truyện online, đọc truyện hay')
@section('title_ab', 'Danh sách truyện')
@section('description', 'Đọc truyện online, đọc truyện hay')
@section('keyword', 'Đọc truyện online, đọc truyện hay')
@section('style')

@endsection
@section('body_id', 'body_home')
@section('content')

    @include('frontsite.home.elements.hot-stories')
    <div class="container" id="list-index">
        <div class="row text-center"></div>
        <div class="hide" id="history-holder"></div>
        <div class="list list-truyen list-new col-xs-12 col-sm-12 col-md-8 col-truyen-main">
            <div class="title-list">
                <h2><a href="{{ route('listStories', ['slug' => 'truyen-moi']) }}/" title="Truyện mới">Truyện mới cập nhật</a></h2><a
                    href="danh-sach/truyen-moi/index.html" title="Truyện mới"><span
                        class="glyphicon glyphicon-menu-right"></span></a>
                <select id="new-select" class="form-control new-select" aria-label="Chọn thể loại">
                    <option value="all">Tất cả</option>
                    @foreach ($categories as $key => $category)
                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                    @endforeach
                </select>
            </div>
            @include('frontsite.home.elements.lastest-stories')
        </div>
        @include('frontsite.home.elements.genres-left-sidebar')
    </div>
    @include('frontsite.home.elements.full-stories')
@endsection
@section('scrypt')

@endsection
