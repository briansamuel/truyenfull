<!DOCTYPE html>
<html lang="vi">



<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile#">
    <meta charset="UTF-8">
    <title>@yield('title', 'Đọc truyện online, đọc truyện hay') - TruyenVV.net</title>
    <base href="{{ url('') }}" />
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <meta name="description" content="@yield('description', 'Đọc truyện online, đọc truyện chữ, truyện hay, truyện full. Truyện Full luôn tổng hợp và cập nhật các chương truyện một cách nhanh nhất.')">
    <meta name="keywords" content="@yield('title', 'doc truyen, doc truyen online, truyen hay, truyen chu')">

    @include('frontsite.elements.seo')
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <!--[if lt IE 9]><script src="lib/html5.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="lib/css3-mediaqueries.min.js"></script><![endif]-->
    <link rel="canonical" href="{{ url('') }}">
    <link rel="preconnect" href="">
    <link rel="preload" as="font" crossorigin="crossorigin" type="font/woff" href="{{ url('') }}/font/Roboto-Regular.woff">
    <link rel="preload" as="font" crossorigin="crossorigin" type="font/woff" href="{{ url('') }}/font/RobotoCondensed-Regular.woff">
    <link rel="preload" as="font" crossorigin="crossorigin" type="font/woff2" href="{{ url('') }}/bootstrap/fonts/glyphicons-halflings-regular.woff2">
    <link rel="preload" as="image" type="image/jpeg" href="img/bg.jpg">
    <link rel="preload" as="image" type="image/png" href="img/spriteimg_new_white_op.png">
    <link rel="preload" as="style" type="text/css" href="css/main.min.css" />
    <link rel="preload" as="script" href="js/main.min.js">
    
    <!-- <link rel="preload" as="script" href="js/analytics.js"> -->
    @yield('style')

    <link rel="preload" as="image" type="image/gif" href="img/pixel-cover.gif">
    <link rel="preload" as="image" type="image/png" href="img/full-label.png">
    @include('frontsite.elements.style')
    <link rel="search" type="application/opensearchdescription+xml" href="xml/opensearch.xml" title="Search">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="icon" type="image/png" href="favicons/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicons/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon-180x180.png">

</head>

<body id="@yield('body_id')" class="@yield('body_class')">
    <div id="wrap">
        <!-- Header -->
        @include('frontsite.header')
        <!-- End Header -->
        @yield('content')
        
        
    </div>
    @include('frontsite.footer')
</body>

</html>