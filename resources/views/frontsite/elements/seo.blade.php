<meta name="twitter:title"              content="@yield('title', 'Đọc truyện online, đọc truyện hay')" />
<meta name="twitter:description"        content="@yield('description', 'Đọc truyện online, đọc truyện chữ, truyện hay, truyện full. Truyện Full luôn tổng hợp và cập nhật các chương truyện một cách nhanh nhất.')" />
<meta name="twitter:image"              content="@yield('image', url('img/logo.png'))" />
<meta property="og:url"                content="{{ Request::url() }}" />
<meta property="og:title"              content="@yield('title', 'Đọc truyện online, đọc truyện hay')" />
<meta property="og:description"        content="@yield('description', 'Đọc truyện online, đọc truyện chữ, truyện hay, truyện full. Truyện Full luôn tổng hợp và cập nhật các chương truyện một cách nhanh nhất.')" />
<meta property="og:image"              content="@yield('image', url('img/logo.png'))" />