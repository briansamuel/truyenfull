@extends('frontsite.index') 
@section('title', 'Tìm truyện với từ khoá'.$keyword)
@section('description', 'Tìm truyện với từ khoá'.$keyword)
@section('keyword', 'Tìm truyện với từ khoá'.$keyword)

@section('style') 
@endsection @section('body_id', 'body_cat') 
@section('content')
<div class="container" id="list-page">
    <div class="col-xs-12 col-sm-12 col-md-9 col-truyen-main">
        <div class="text-center"></div>
        <div class="list list-truyen col-xs-12">
            <div class="title-list">
                <h2>TÌM TRUYỆN VỚI TỪ KHOÁ {{ $keyword }}</h2>
              
            </div>
           
            @foreach($stories as $story)
            <div class="row" itemscope="" itemtype="https://schema.org/Book">
                <div class="col-xs-3">
                    <div>
                        <div data-image="{{ $story->story_thumbnail ? str_replace('uploads/', 'cover/', $story->story_thumbnail) : 'cover/files/default-thumbnail.jpg' }}" data-desk-image="{{ $story->story_thumbnail ? str_replace('uploads/', 'cover/', $story->story_thumbnail) : 'cover/files/default-thumbnail.jpg' }}" data-classname="cover" data-alt="{{ $story->story_title }}" class="lazyimg"></div>
                    </div>
                </div>
                <div class="col-xs-7">
                    <div><span class="glyphicon glyphicon-book"></span>
                        <h3 class="truyen-title" itemprop="name"><a href="{{ $story->story_slug }}" title="{{ $story->story_title }}" itemprop="url">{{ $story->story_title }}</a></h3><span class="author" itemprop="author"><span class="glyphicon glyphicon-pencil"></span> {{ $story->story_author }}</span>
                    </div>
                </div>
                <div class="col-xs-2 text-info">
                    
                    @if(isset($story->lastest_chapter))
                    
                    <div><a href="{{ $story->story_slug }}/{{ $story->lastest_chapter->chapter_slug }}/" title="{{ $story->story_title }} - Chương {{ $story->lastest_chapter->chapter_order }}"><span class="chapter-text"><span>Chương </span></span>{{ $story->lastest_chapter->chapter_order }}</a></div>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="visible-md-block visible-lg-block col-md-3 text-center col-truyen-side">
        <div class="panel cat-desc text-left">
            <div class="panel-body">
                Danh sách truyện có liên quan từ khoá {{ $keyword }}
            </div>
        </div>
        <div class="list list-truyen list-cat col-xs-12">
            <div class="title-list">
                <h4>Thể loại truyện</h4>
            </div>
            <div class="row">
                @foreach($categories as $key => $c) 
                    <div class="col-xs-6"><a href="the-loai/{{ $c->category_slug }}/" title="{{ $c->category_seo_title }}">{{ $c->category_name }}</a></div>
                @endforeach
            </div>
        </div>
        <div class="list list-truyen list-side col-xs-12">
            <div class="title-list">
                <h4>Truyện Đang Hot</h4>
            </div>
            <div class="row top-nav" data-limit="10" data-cat="1">
                <div class="col-xs-4 active" data-type="day">Ngày</div>
                <div class="col-xs-4" data-type="month">Tháng</div>
                <div class="col-xs-4" data-type="all">All time</div>
            </div>
            @foreach($top_days_stories as $k => $s)
            <div class="row top-item">
                <div class="col-xs-12">
                    <div class="top-num top-{{ $k+1 }}">{{ $k+1 }}</div>
                    <div class="s-title">
                        <h3><a href="{{ url($s->story_slug) }}" title="{{ $s->story_title }}">{{ $s->story_title }}</a></h3>
                    </div>
                    <div>{!! $s->story_genres !!}</div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="container text-center pagination-container">
    <div class="col-xs-12 col-sm-12 col-md-9 col-truyen-main">
        {!! $page_list !!}
    </div>
</div>
@endsection 
@section('scrypt')
@endsection