@extends('frontsite.index')
@section('style')
@endsection
@section('title', $story->story_title)
@section('description', "Đọc truyện {$story->story_title} của tác giả {$story->story_author} thuộc hệ thống truyện truyenvv.net")
@section('keyword', $story->story_seo_keyword)
@section('image', url($story->story_thumbnail))
@section('body_id', 'body_truyen')
@section('content')
<div class="container csstransforms3d" id="truyen">
    <div class="col-xs-12 col-sm-12 col-md-9 col-truyen-main">
        <div class="col-xs-12 col-info-desc" itemscope itemtype="https://schema.org/Book" xmlns:v="http://rdf.data-vocabulary.org/#" typeof="v:Review-aggregate">
            <div class="title-list book-intro">
                <h2>Thông tin truyện</h2>
            </div>
            <h3 class="title" itemprop="name" property="v:itemreviewed">{{ $story->story_title }}</h3>
            <div class="col-xs-12 col-sm-4 col-md-4 info-holder">
                <div class="books">
                    <div class="book"><img src="{{ $story->story_thumbnail }}" alt="{{ $story->story_title }}" itemprop="image"></div>
                </div>
                <div class="info">
                    <div>
                        <h3>Tác giả:</h3><a itemprop="author" href="{{ Str::slug($story->story_author) }}" title="Slaydark">{{ $story->story_author }}</a>
                    </div>
                    <div>
                        <h3>Thể loại:</h3>
                        {!! $story->story_genres !!}
                    </div>
                    <div>
                        <h3>Nguồn:</h3><span class="source">{{ $story->story_source }}</span>
                    </div>
                    <div>
                        <h3>Trạng thái:</h3><span class="text-primary">{{ $story->story_status }}</span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 desc">
                <div class="rate">
                    <div class="rate-holder" data-score="9.1"></div><em class="rate-text"></em>
                    <div class="small"><em>Đánh giá: <span typeof="v:Rating" rel="v:rating"><strong><span property="v:average">9.1</span></strong>/<span property="v:best">10</span></span> từ <strong><span property="v:count">2265</span> lượt</strong></em></div>
                </div>
                <div class="desc-text" itemprop="description">
                    {!! $story->story_description !!}
                </div>
                <div class="showmore"><a class="btn btn-default btn-xs hide" href="javascript:void(0)" title="Xem thêm">Xem thêm »</a></div>
                <div class="l-chapter">
                    <div class="l-title">
                        <h3>Các chương mới nhất</h3>
                    </div>
                    <ul class="l-chapters">
                        @foreach($lastest_chapters as $chapter)
                        <li><span class="glyphicon glyphicon-certificate"></span> <a href="{{ $story->story_slug }}/{{ $chapter->chapter_slug }}/" title="{{ strip_tags($chapter->chapter_title) }}"><span class="chapter-text">{{ strip_tags($chapter->chapter_title) }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12" id="list-chapter">
            <div class="title-list">
                <h2>Danh sách chương</h2>
            </div>
            <div class="row">
                @include('frontsite.stories.elements.list-chapter')
            </div>
            <input id="truyen-id" type="hidden" value="{{ $story->id }}">
            <input id="total-page" type="hidden" value="{{ $pagination->total() }}">
            <input id="truyen-ascii" type="hidden" value="{{ $story->story_slug }}">
            <input id="truyen-comment" type="hidden" value="{{ $story->story_slug }}">
            <input id="chapter-sac" type="hidden" value="1">
            {{ $pagination->links() }}
            <input name="truyen" type="hidden" value="12-nu-than">
        </div>
        <div class="col-xs-12 comment-box">
            <div class="title-list">
                <h2>Bình luận truyện</h2>
            </div>
            <button id="view-comments" class="btn btn-info hidden-md hidden-lg">Mở bình luận</button>
            <div class="col-xs-12">
                <div class="row" id="fb-comment-story"></div>
            </div>
        </div>
    </div>
    <div class="visible-md-block visible-lg-block col-md-3 text-center col-truyen-side">
        <div class="list list-truyen list-side col-xs-12">
            <div class="title-list">
                <h4>Truyện đang hot</h4>
            </div>
            <div class="row top-nav" data-limit="10">
                <div class="col-xs-4 active" data-type="day">Ngày</div>
                <div class="col-xs-4" data-type="month">Tháng</div>
                <div class="col-xs-4" data-type="all">All time</div>
            </div>
            @foreach($top_days_stories as $k => $s)
            <div class="row top-item">
                <div class="col-xs-12">
                    <div class="top-num top-{{ $k+1 }}">{{ $k+1 }}</div>
                    <div class="s-title">
                        <h3><a href="{{ url($s->story_slug) }}" title="{{ $s->story_title }}">{{ $s->story_title }}</a></h3>
                    </div>
                    <div>{!! $s->story_genres !!}</div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('scrypt')

@endsection