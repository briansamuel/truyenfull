<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <title>Error 404 (Not Found)!!</title>
    <meta name="keywords" content="404,error,not found">
    <meta name="description" content="Error 404 (Not Found)">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="{{ url('') }}/css/error.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('') }}/favicon.ico">
</head>

<body class="vsc-initialized"> <a href="{{ url('') }}" title="Trở về trang chủ"><img
            src="{{ url('') }}/img/logo.png" alt="Trở về trang chủ"></a>
    <p><b>404.</b> <ins>Đường dẫn không tồn tại.</ins></p>
    <p>Có thể truyện bạn muốn xem vì lý do nào đó mà đã bị thay đổi link, hãy <a href="{{ url('') }}"
            title="Trở về trang chủ">trở về trang chủ</a> và thử tìm kiếm với tên truyện đó. Chúng mình rất xin lỗi vì
        sự bất tiện này!</p>
   
    
</body>

</html>
