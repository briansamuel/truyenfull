<div id="footer" class="footer">
    <div class="container">
        <div class="hidden-xs col-sm-5"><strong>Truyện Full</strong> - <a href="index.html" title="Đọc truyện online">Đọc truyện</a> online, <a href="index.html" title="Đọc truyện chữ">đọc truyện</a> chữ, <a href="index.html" title="Truyện hay">truyện hay</a>. Website luôn cập nhật những bộ <a href="danh-sach/truyen-moi/index.html" title="Truyện mới">truyện mới</a> thuộc các thể loại đặc sắc như <a href="the-loai/tien-hiep/index.html" title="Truyện tiên hiệp">truyện tiên hiệp</a>, <a href="the-loai/kiem-hiep/index.html" title="Truyện kiếm hiệp">truyện kiếm hiệp</a>, hay <a href="the-loai/ngon-tinh/index.html" title="Truyện ngôn tình">truyện ngôn tình</a> một cách nhanh nhất. Hỗ trợ mọi thiết bị như di động và máy tính bảng.</div>
        <ul class="col-xs-12 col-sm-7 list-unstyled">
            <li class="text-right pull-right"><a href="contact/index.html" title="Contact">Contact</a> - <a href="tos/index.html" title="Terms of Service">ToS</a><a class="backtop" title="Trở lên đầu trang" href="#wrap" rel="nofollow" aria-label="Trở về đầu trang"><span class="glyphicon glyphicon-upload"></span></a> </li>
            <li class="hidden-xs tag-list"><a href="danh-sach/dam-my-hai/index.html" title="đam mỹ hài">đam mỹ hài</a> <a href="danh-sach/ngon-tinh-hai/index.html" title="ngôn tình hài">ngôn tình hài</a> <a href="danh-sach/truyen-teen-hay/index.html" title="truyện teen hay">truyện teen hay</a> <a href="danh-sach/ngon-tinh-hay/index.html" title="ngôn tình hay">ngôn tình hay</a> <a href="the-loai/dam-my/index.html" title="truyện đam mỹ">truyện đam mỹ</a> <a href="the-loai/ngon-tinh/hoan/index.html" title="truyện ngôn tình">truyện ngôn tình</a> <a href="the-loai/ngon-tinh/hoan/index.html" title="ngôn tình hoàn">ngôn tình hoàn</a> <a href="danh-sach/ngon-tinh-nguoc/index.html" title="ngôn tình ngược">ngôn tình ngược</a> <a href="danh-sach/kiem-hiep-hay/index.html" title="truyện kiếm hiệp hay">truyện kiếm hiệp hay</a> <a href="danh-sach/tien-hiep-hay/index.html" title="truyện tiên hiệp hay">truyện tiên hiệp hay</a> <a href="danh-sach/ngon-tinh-sung/index.html" title="ngôn tình sủng">ngôn tình sủng</a></li>
        </ul>
    </div>
</div>
<script src="js/main.min.js"></script>
<noscript>
    <link rel="stylesheet" type="text/css" href="css/main.min.css" />
</noscript>
<script>
    $('head').append('<link rel="stylesheet" type="text/css" href="css/main.min.css"/>');
</script>
@yield('script')