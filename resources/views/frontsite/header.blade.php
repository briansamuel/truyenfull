<div class="navbar navbar-default navbar-static-top" role="navigation" id="nav">
    <script type="text/javascript">
        function getCookie(d) {
            d += "=";
            for (var b = decodeURIComponent(document.cookie).split(";"), c = [], a = 0; a < b.length; a++) 0 == b[a]
                .trim().indexOf(d) && (c = b[a].trim().split("="));
            return 0 < c.length ? c[1] : ""
        }
        var js_bgcolor = getCookie("bgcolor-cookie");
        "#232323" == js_bgcolor && (document.getElementsByTagName("body")[0].className += " dark-theme");

    </script>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span
                    class="sr-only">Hiện menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span
                    class="icon-bar"></span></button>
            <h1><a class="header-logo" href="{{ asset('') }}" title="doc truyen">doc truyen</a></h1>
        </div>
        <div class="navbar-collapse collapse" itemscope itemtype="https://schema.org/WebSite">
            <meta itemprop="url" content="{{ asset('') }}" />
            <ul class="control nav navbar-nav ">
                <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><span
                            class="glyphicon glyphicon-list"></span> Danh sách <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="danh-sach/truyen-moi/" title="Truyện mới cập nhật">Truyện mới cập nhật</a></li>
                        <li><a href="danh-sach/truyen-hot/" title="Truyện Hot">Truyện Hot</a></li>
                        <li><a href="danh-sach/truyen-full/" title="Truyện Full">Truyện Full</a></li>
                        <li><a href="danh-sach/tien-hiep-hay/" title="Tiên Hiệp Hay">Tiên Hiệp Hay</a></li>
                        <li><a href="danh-sach/kiem-hiep-hay/" title="Kiếm Hiệp Hay">Kiếm Hiệp Hay</a></li>

                    </ul>
                </li>
                <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><span
                            class="glyphicon glyphicon-list"></span> Thể loại <span class="caret"></span></a>
                    <div class="dropdown-menu multi-column">
                        <div class="row">
                            <div class="col-md-4">
                                <ul class="dropdown-menu">
                                    @foreach ($categories as $key => $category)
                                        @if ($key >= 0 && $key < 12)
                                            <li><a href="the-loai/{{ $category->category_slug }}/"
                                                    title="{{ $category->category_seo_title }}">{{ $category->category_name }}</a>
                                            </li>
                                        @endif
                                    @endforeach

                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul class="dropdown-menu">
                                    @foreach ($categories as $key => $category)
                                        @if ($key >= 12 && $key < 24)
                                            <li><a href="the-loai/{{ $category->category_slug }}/"
                                                    title="{{ $category->category_seo_title }}">{{ $category->category_name }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul class="dropdown-menu">
                                    @foreach ($categories as $key => $category)
                                        @if ($key >= 24 && $key < 36)
                                            <li><a href="the-loai/{{ $category->category_slug }}/"
                                                    title="{{ $category->category_seo_title }}">{{ $category->category_name }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><span
                            class="glyphicon glyphicon-cog"></span> Tùy chỉnh <span class="caret"></span></a>
                    <div class="dropdown-menu dropdown-menu-right settings">
                        <form class="form-horizontal">
                            <div class="form-group form-group-sm"><label class="col-sm-2 col-md-5 control-label"
                                    for="truyen-background">Màu nền</label>
                                <div class="col-sm-5 col-md-7"><select class="form-control" id="truyen-background">
                                        <option value="#F4F4F4">Xám nhạt</option>
                                        <option value="#E9EBEE">Xanh nhạt</option>
                                        <option value="#F4F4E4">Vàng nhạt</option>
                                        <option value="#EAE4D3">Màu sepia</option>
                                        <option value="#D5D8DC">Xanh đậm</option>
                                        <option value="#FAFAC8">Vàng đậm</option>
                                        <option value="#EFEFAB">Vàng ố</option>
                                        <option value="#FFF">Màu trắng</option>
                                        <option value="hatsan">Hạt sạn</option>
                                        <option value="sachcu">Sách cũ</option>
                                        <option value="#232323">Màu tối</option>
                                    </select></div>
                            </div>
                            <div class="form-group form-group-sm"><label class="col-sm-2 col-md-5 control-label"
                                    for="font-chu">Font chữ</label>
                                <div class="col-sm-5 col-md-7"><select class="form-control" id="font-chu">
                                        <option value="'Palatino Linotype', serif">Palatino Linotype</option>
                                        <option value="Bookerly, serif">Bookerly</option>
                                        <option value="Minion, serif">Minion</option>
                                        <option value="'Segoe UI', sans-serif">Segoe UI</option>
                                        <option value="Roboto, sans-serif">Roboto</option>
                                        <option value="'Roboto Condensed', sans-serif">Roboto Condensed</option>
                                        <option value="'Patrick Hand', sans-serif">Patrick Hand</option>
                                        <option value="'Noticia Text', sans-serif">Noticia Text</option>
                                        <option value="'Times New Roman', serif">Times New Roman</option>
                                        <option value="Verdana, sans-serif">Verdana</option>
                                        <option value="Tahoma, sans-serif">Tahoma</option>
                                        <option value="Arial, sans-serif">Arial</option>
                                    </select></div>
                            </div>
                            <div class="form-group form-group-sm"><label class="col-sm-2 col-md-5 control-label"
                                    for="size-chu">Size chữ</label>
                                <div class="col-sm-5 col-md-7"><select class="form-control" id="size-chu">
                                        <option value="16px">16</option>
                                        <option value="18px">18</option>
                                        <option value="20px">20</option>
                                        <option value="22px">22</option>
                                        <option value="24px">24</option>
                                        <option value="26px">26</option>
                                        <option value="28px">28</option>
                                        <option value="30px">30</option>
                                        <option value="32px">32</option>
                                        <option value="34px">34</option>
                                        <option value="36px">36</option>
                                        <option value="38px">38</option>
                                        <option value="40px">40</option>
                                    </select></div>
                            </div>
                            <div class="form-group form-group-sm"><label class="col-sm-2 col-md-5 control-label"
                                    for="line-height">Chiều cao dòng</label>
                                <div class="col-sm-5 col-md-7"><select class="form-control" id="line-height">
                                        <option value="100%">100%</option>
                                        <option value="120%">120%</option>
                                        <option value="140%">140%</option>
                                        <option value="160%">160%</option>
                                        <option value="180%">180%</option>
                                        <option value="200%">200%</option>
                                    </select></div>
                            </div>
                            <div class="form-group form-group-sm"><label class="col-sm-2 col-md-5 control-label">Full
                                    khung</label>
                                <div class="col-sm-5 col-md-7"><label class="radio-inline" for="fluid-yes"><input
                                            type="radio" name="fluid-switch" id="fluid-yes" value="yes">
                                        Có</label><label class="radio-inline" for="fluid-no"><input type="radio"
                                            name="fluid-switch" id="fluid-no" value="no" checked=""> Không</label></div>
                            </div>
                        </form>
                    </div>
                </li>
            </ul>
            <form class="navbar-form navbar-right" action="{{ asset('') }}/tim-kiem/" role="search"
                itemprop="potentialAction" itemscope itemtype="https://schema.org/SearchAction">
                <div class="input-group search-holder">
                    <meta itemprop="target" content="tim-kiem/index1bc4.html?tukhoa={tukhoa}" />
                    <input aria-label="Từ khóa tìm kiếm" role="search key" class="form-control" id="search-input"
                        type="search" name="tukhoa" placeholder="Tìm kiếm..." value="" itemprop="query-input" required>
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit" aria-label="Tìm kiếm" role="search"><span
                                class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
                <div class="list-group list-search-res hide"></div>
            </form>
            <div id="login-status" class="hide"></div>
        </div>
    </div>
    <div class="navbar-breadcrumb">
        <div class="container breadcrumb-container"> Đọc truyện online, đọc truyện chữ, truyện full, truyện hay. Tổng
            hợp đầy đủ và cập nhật liên tục. </div>
    </div>
</div>
