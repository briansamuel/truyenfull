<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Services\CategoryService;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   

        $categories = CategoryService::getAllByKey(['id', 'category_name', 'category_slug', 'category_description', 'category_seo_title'], null);
        View::share('categories', $categories);
        //
        Schema::defaultStringLength(191);
    }
}
