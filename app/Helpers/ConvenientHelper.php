<?php

namespace App\Helpers;

class ConvenientHelper {

    public static function getConvenient() {

        $convenients = [
            'service_hotel' => [
                0 => "Nhân viên xách hành lý",
                1 => "Thức uống chào mừng miễn phí",
                2 => "Dịch vụ concierge/hỗ trợ khách",
                3 => "Dịch vụ thu đổi ngoại tệ",
                4 => "Người gác cửa",
                5 => "EARLY_CHECK_IN",
                6 => "Dịch vụ nhận phòng cấp tốc",
                7 => "Dịch vụ trả phòng cấp tốc",
                8 => "Quầy lễ tân",
                9 => "Lễ tân 24h",
                10 => "Bảo vệ 24 giờ",
                11 => "Dịch vụ trả phòng muộn",
                12 => "Dịch vụ giặt ủi",
                13 => "Dịch vụ lưu trữ/bảo quản hành lý",
                14 => "Dịch vụ chăm sóc y tế",
                15 => "Nhật báo tại sảnh",
                16 => "Nhật báo",
                17 => "Nhân viên gác cổng",
                18 => "Dịch vụ hỗ trợ đặt Tour",
            ],

            'foods' => [
                19 => "Bữa sáng với thực đơn gọi món",
                20 => "Quầy bar",
                21 => "Bữa sáng",
                22 => "Bữa sáng món tự chọn",
                23 => "Tiệm cà phê",
                24 => "Khu ẩm thực",
                25 => "Bữa tối với thực đơn chọn sẵn",
                26 => "Bữa trưa với thực đơn chọn sẵn",
                27 => "Đồ ăn nhẹ",
                28 => "Phục vụ món chay",
            ],

            'convenient_room' => [
                29 => "Áo choàng tắm",
                30 => "Bồn tắm",
                31 => "Truyền hình cáp",
                32 => "Bàn làm việc",
                33 => "Máy sấy tóc",
                34 => "Két an toàn trong phòng",
                35 => "Nhà bếp mini",
                36 => "Minibar",
                37 => "Phòng tắm vòi sen",
                38 => "TV",
            ],

            'convenient_general' => [
                39 => "Kết nối wifi",
                40 => "Máy lạnh",
                41 => "Tiệc chiêu đãi",
                42 => "Phòng giữ đồ",
                43 => "Phòng gia đình",
                44 => "Phòng không hút thuốc",
                45 => "Hồ bơi",
                46 => "Sân thượng/Sân hiên",
                47 => "Không khói thuốc",
                48 => "Sân thượng/sân hiên",
            ],

            'convenient_public' => [
                49 => "Bãi đậu xe",
                50 => "Cà phê/trà tại sảnh",
                51 => "Nhận phòng sớm",
                52 => "Thang máy",
                53 => "Trả phòng muộn",
                54 => "Nhà hàng",
                55 => "Dịch vụ dọn phòng",
                56 => "Két an toàn",
                57 => "WiFi tại khu vực chung",
            ],

            'convenient_office' => [
                58 => "Dịch vụ văn phòng",
                59 => "Các tiện nghi văn phòng",
                60 => "Máy tính",
                61 => "Lễ tân hội nghị",
                62 => "Phòng hội nghị",
                63 => "Tiện nghi phục vụ hội họp",
                64 => "Máy photocopy",
                65 => "Máy chiếu",
            ],

            'convenient_activaty' => [
                66 => "Hồ bơi trong nhà",
                67 => "Hồ bơi ngoài trời",
                68 => "Karaoke",
                69 => "Mát-xa",
                70 => "Dù (ô) che nắng",
                71 => "Dịch vụ spa",
                72 => "Xông hơi khô/ướt",
                73 => "Khu vực vui chơi trẻ em",
            ],

            'convenient_transport' => [
                74 => "Đưa đón sân bay",
                75 => "Cho thuê xe máy",
                76 => "Cho thuê xe hơi",
                77 => "Bãi đậu xe hạn chế",
            ],
            
            'support_people' => [
                78 => "Thuận tiện cho người khuyết tật",
                79 => "Lối đi lại cho người khuyết tật",
                80 => "Phù hợp cho xe lăn",
                
            ]
        ];
        return $convenients;
    }
}