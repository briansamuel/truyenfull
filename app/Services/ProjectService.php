<?php
namespace App\Services;

use App\Models\ProjectModel;
use App\Transformers\AgencyTransformer;
use Illuminate\Support\Str;

class ProjectService
{
	public static function totalRows($params) {
        $result = ProjectModel::totalRows($params);
        return $result;
	}
	
	public static function getMany($limit, $offset, $filter)
	{
		$result = ProjectModel::getMany($limit, $offset, $filter);
        return $result ? $result : [];
	}
	public static function findByKey($key, $value)
	{
        $result = ProjectModel::findByKey($key, $value);
        return $result ? $result : [];
	}
	public function insert($params)
	{
		$insert['project_title'] = $params['project_title']; 
		$insert['project_slug'] = isset($params['project_slug']) ? $params['project_slug'] : Str::slug($params['project_title']);
		$insert['project_description'] = $params['project_description']; 
        $insert['project_content'] = $params['project_content'];
        $insert['project_location'] = $params['project_location']; 
		$insert['project_seo_title'] = isset($params['project_seo_title']) ? $params['project_seo_title'] : '';
		$insert['project_seo_description'] = isset($params['project_seo_description']) ? $params['project_seo_description'] : ''; 
		$insert['project_seo_keyword'] = isset($params['project_seo_keyword']) ? $params['project_seo_keyword'] : ''; 
		$insert['project_thumbnail'] = isset($params['project_thumbnail']) ? $params['project_thumbnail'] : ''; 
		$insert['project_author'] = isset($params['project_author']) ? $params['project_author'] : ''; 
		$insert['project_status'] = $params['project_status']; 
		$insert['language'] = isset($params['language']) ? $params['language'] : 'vi'; 
		$insert['created_by_user'] = isset($params['created_by_user']) ? $params['created_by_user'] : 0; 
		$insert['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$insert['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$insert['updated_at'] = date("Y-m-d H:i:s"); 
		return ProjectModel::insert($insert);		
	}
	public function update($id, $params)
	{
		$update['project_title'] = $params['project_title']; 
		$update['project_slug'] = isset($params['project_slug']) ? $params['project_slug'] : Str::slug($params['project_title']);
		$update['project_description'] = $params['project_description']; 
        $update['project_content'] = $params['project_content'];
        $update['project_location'] = $params['project_location']; 
		$update['project_seo_title'] = isset($params['project_seo_title']) ? $params['project_seo_title'] : '';
		$update['project_seo_description'] = isset($params['project_seo_description']) ? $params['project_seo_description'] : ''; 
		$update['project_seo_keyword'] = isset($params['project_seo_keyword']) ? $params['project_seo_keyword'] : ''; 
		$update['project_thumbnail'] = isset($params['project_thumbnail']) ? $params['project_thumbnail'] : ''; 
		$update['project_status'] = $params['project_status']; 
		$update['language'] = isset($params['language']) ? $params['language'] : 'vi'; 
		$update['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$update['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$update['updated_at'] = date("Y-m-d H:i:s"); 
		return ProjectModel::update($id, $update);		
	}

	public function updateMany($ids, $data)
    {
        return ProjectModel::updateManyProject($ids, $data);
	}
	
	public function deleteMany($ids)
    {
        return ProjectModel::deleteManyProject($ids);
	}
	
	public function delete($id)
	{
		return ProjectModel::delete($id);		
	}

	public function getList(array $params)
    {
		$params['query'] = isset($params['query']) ? $params['query'] : '';
        $total = self::totalRows($params['query']);
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$column = ['id', 'project_title', 'project_status', 'language', 'created_at', 'project_thumbnail'];
        $result = ProjectModel::getMany($column, $pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}
	
	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}

	public static function takeNew($quantity, $filter)
    {
        return ProjectModel::takeNew($quantity, $filter);
    }
}