<?php
namespace App\Services;
use App\Models\SettingModel;
use Auth;

class SettingService
{
    public static function add($data)
    {
        $user_id = Auth::guard('admin')->user()->id;
        foreach($data as $key=>$value) {
            $param = [];
            if($detail = SettingModel::findByName($key)) {
                $param['setting_value'] = $value;

                SettingModel::update($detail->id, $param);
            } else {
                $param['setting_key'] = $key;
                $param['setting_value'] = $value;
                $param['created_by_user'] = $user_id;
                $param['updated_by_user'] = $user_id;
                $param['created_at'] = date("Y-m-d H:i:s");
                $param['updated_at'] = date("Y-m-d H:i:s");

                SettingModel::add($param);
            }
        }

        return true;
    }

    public static function savedSettings()
    {
        $dbSettings = SettingModel::all();
        return $dbSettings;
    }

    public static function deleteGroup($group)
    {
        $conditions = [
            ['setting_key', 'like', "%$group%"]
        ];
        return SettingModel::deletebyCondition($conditions);
    }

    /*
     *
     */

    public static function get($key, $default = '')
    {
        if($setting = SettingModel::findByName($key)){
            return $setting->setting_value;
        }

        return $default;
    }

    public static function getSetting($type)
    {
        $dbSettings = SettingModel::get($type);
        return $dbSettings;
    }
}