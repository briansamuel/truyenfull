<?php
namespace App\Services;

use App\Models\StoryModel;
use App\Transformers\AgencyTransformer;
use Illuminate\Support\Str;

class StoryService
{
	public static function totalRows($params) {
        $result = StoryModel::totalRows($params);
        return $result;
	}
	
	public static function getMany($column, $pagination, $sort, $query)
	{
		$result = StoryModel::getMany($column, $pagination, $sort, $query);
        return $result ? $result : [];
	}
	
	public static function getManyWithPaginate($column, $pagination, $sort, $query)
	{
		$result = StoryModel::getManyWithPaginate($column, $pagination, $sort, $query);
        return $result ? $result : [];
	}

	public static function getManyByCategory($column, $pagination, $sort, $query)
	{
		$result = StoryModel::getManyByCategory($column, $pagination, $sort, $query);
        return $result ? $result : [];
	}

	public static function findByKey($key, $value, $column = ['*'], $with = null)
	{
        $result = StoryModel::findByKey($key, $value, $column , $with);
        return $result ? $result : [];
	}

	
	public static function insert($params)
	{
		$insert['story_title'] = $params['story_title']; 
		$insert['story_slug'] = isset($params['story_slug']) ? $params['story_slug'] : Str::slug($params['story_title']);
		$insert['story_description'] = $params['story_description']; 
		$insert['story_seo_title'] = isset($params['story_seo_title']) ? $params['story_seo_title'] : '';
		$insert['story_seo_description'] = isset($params['story_seo_description']) ? $params['story_seo_description'] : ''; 
		$insert['story_seo_keyword'] = isset($params['story_seo_keyword']) ? $params['story_seo_keyword'] : ''; 
		$insert['story_thumbnail'] = isset($params['story_thumbnail']) ? $params['story_thumbnail'] : ''; 
		$insert['story_author'] = isset($params['story_author']) ? $params['story_author'] : 0; 
		$insert['story_status'] = $params['story_status']; 
		$insert['story_source'] = isset($params['story_source']) ? $params['story_source'] : '';
		$insert['story_feature'] = isset($params['story_feature']) ? $params['story_feature'] : 0;
		$insert['language'] = isset($params['language']) ? $params['language'] : 'vi'; 
		$insert['created_by_user'] = isset($params['created_by_user']) ? $params['created_by_user'] : 0; 
		$insert['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$insert['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$insert['updated_at'] = date("Y-m-d H:i:s"); 
		return StoryModel::insert($insert);		
	}
	public static function update($id, $params)
	{
		$update['story_title'] = $params['story_title']; 
		$update['story_slug'] = isset($params['story_slug']) ? $params['story_slug'] : Str::slug($params['story_title']);
		$update['story_description'] = $params['story_description']; 
		$update['story_seo_title'] = isset($params['story_seo_title']) ? $params['story_seo_title'] : '';
		$update['story_seo_description'] = isset($params['story_seo_description']) ? $params['story_seo_description'] : ''; 
		$update['story_seo_keyword'] = isset($params['story_seo_keyword']) ? $params['story_seo_keyword'] : ''; 
		$update['story_thumbnail'] = isset($params['story_thumbnail']) ? $params['story_thumbnail'] : ''; 
		$update['story_status'] = $params['story_status']; 
		$update['story_feature'] = isset($params['story_feature']) ? $params['story_feature'] : 0;
		$update['language'] = isset($params['language']) ? $params['language'] : 'vi';
		$update['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$update['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$update['updated_at'] = date("Y-m-d H:i:s"); 
		return StoryModel::update($id, $update);		
	}

	public static function updateOnly($id, $params)
	{
		$params['updated_at'] = date("Y-m-d H:i:s"); 
		return StoryModel::update($id, $params);		
	}

	public static function updateStatusCrawl($id, $params) {
		$update['crawl_status'] = $params['crawl_status']; 
		return StoryModel::update($id, $update);
	}

	public function updateMany($ids, $data)
    {
        return StoryModel::updateManyStory($ids, $data);
	}
	
	public function deleteMany($ids)
    {
        return StoryModel::deleteManyStory($ids);
	}
	
	public function delete($id)
	{
		return StoryModel::delete($id);		
	}

	public function getList(array $params)
    {
		$pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$total = self::totalRows($query);
		
		$column = ['id', 'story_title', 'story_slug', 'story_status', 'language', 'created_at', 'story_author', 'story_source'];
        $result = StoryModel::getMany($column, $pagination, $sort, $query);
		foreach($result as $story) {
            
            $story->total_chapter = ChapterService::totalRows(array('story_id' => $story->id));
        }
        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}
	
	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}

	public static function takeNew($quantity, $filter)
    {
        return StoryModel::takeNew($quantity, $filter);
    }
}