<?php
namespace App\Services;

use App\Models\PageModel;
use Illuminate\Support\Str;

class PageService
{
	public static function totalRows($params) {
        $result = PageModel::totalRows($params);
        return $result;
	}
	
	public function getMany($limit, $offset, $filter)
	{
		$result = PageModel::getMany($limit, $offset, $filter);
        return $result ? $result : [];
	}
	public function findByKey($key, $value)
	{
        $result = PageModel::findByKey($key, $value);
        return $result ? $result : [];
	}
	public function insert($params)
	{
		$insert['page_title'] = $params['page_title']; 
		$insert['page_slug'] = isset($params['page_slug']) ? $params['page_slug'] : Str::slug($params['page_title']);
		$insert['page_description'] = $params['page_description']; 
		$insert['page_content'] = $params['page_content']; 
		$insert['page_seo_title'] = isset($params['page_seo_title']) ? $params['page_seo_title'] : '';
		$insert['page_seo_description'] = isset($params['page_seo_description']) ? $params['page_seo_description'] : ''; 
		$insert['page_seo_keyword'] = isset($params['page_seo_keyword']) ? $params['page_seo_keyword'] : ''; 
		
		$insert['page_author'] = isset($params['page_author']) ? $params['page_author'] : 0; 
		$insert['page_status'] = $params['page_status']; 
		$insert['page_type'] = isset($params['page_type']) ? $params['page_type'] : 'post';
		$insert['language'] = isset($params['language']) ? $params['language'] : 'vi'; 
		$insert['created_by_user'] = isset($params['created_by_user']) ? $params['created_by_user'] : 0; 
		$insert['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$insert['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$insert['updated_at'] = date("Y-m-d H:i:s"); 
		return PageModel::insert($insert);		
	}
	public function update($id, $params)
	{
		$update['page_title'] = $params['page_title']; 
		$update['page_slug'] = isset($params['page_slug']) ? $params['page_slug'] : Str::slug($params['page_title']);
		$update['page_description'] = $params['page_description']; 
		$update['page_content'] = $params['page_content']; 
		$update['page_seo_title'] = isset($params['page_seo_title']) ? $params['page_seo_title'] : '';
		$update['page_seo_description'] = isset($params['page_seo_description']) ? $params['page_seo_description'] : ''; 
		$update['page_seo_keyword'] = isset($params['page_seo_keyword']) ? $params['page_seo_keyword'] : ''; 
		
		$update['page_status'] = $params['page_status']; 
		$update['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$update['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$update['updated_at'] = date("Y-m-d H:i:s"); 
		return PageModel::update($id, $update);		
	}

	public function updateMany($ids, $data)
    {
        return PageModel::updateManyPage($ids, $data);
	}
	
	public function deleteMany($ids)
    {
        return PageModel::deleteManyPage($ids);
	}
	
	public function delete($id)
	{
		return PageModel::delete($id);		
	}

	public function getList(array $params)
    {
        $total = self::totalRows($params['query']);
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$column = ['id', 'page_title', 'page_status', 'language', 'created_at', 'page_author'];
        $result = PageModel::getMany($column, $pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}
	
	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}
}