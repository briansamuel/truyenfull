<?php
namespace App\Services;

use App\Models\ChapterModel;
use App\Transformers\AgencyTransformer;
use Illuminate\Support\Str;

class ChapterService
{
	public static function totalRows($params) {
        $result = ChapterModel::totalRows($params);
        return $result;
	}
	
	public static function getMany($column, $limit, $offset, $filter)
	{
		$result = ChapterModel::getMany($column, $limit, $offset, $filter);
        return $result ? $result : [];
	}

	public static function getLastestChapter($column, $limit, $offset, $filter)
	{
		$result = ChapterModel::getLastestChapter($column, $limit, $offset, $filter);
        return $result ? $result : [];
	}

	public static function getLastestChapterPaginate($column, $limit, $offset, $filter)
	{
		$result = ChapterModel::getLastestChapterPaginate($column, $limit, $offset, $filter);
        return $result ? $result : [];
	}


	public static function getChapters($columns, $pagination, $sort, $filter, $group)
	{
		$result = ChapterModel::getChapters($columns, $pagination, $sort, $filter, $group);
        return $result ? $result : [];
	}

	public static function findByKey($key, $value)
	{
        $result = ChapterModel::findByKey($key, $value);
        return $result ? $result : [];
	}

	public static function findByKeyLast($key, $value, $column) {
		$result = ChapterModel::findByKeyLast($key, $value, $column);
        return $result ? $result : [];
	}
	public static function findByParams($columns, $filter)
	{
        $result = ChapterModel::findByParams($columns, $filter);
        return $result ? $result : [];
	}

	public static function nextChapter($columns, $filter) {
		$result = ChapterModel::nextChapter($columns, $filter);
        return $result ? $result : [];
	}

	public static function previousChapter($columns, $filter) {
		$result = ChapterModel::previousChapter($columns, $filter);
        return $result ? $result : [];
	}

	public static function insert($params)
	{
		$insert['chapter_title'] = $params['chapter_title'];
		$insert['story_id'] = isset($params['story_id']) ? $params['story_id'] : 0; 
		$insert['chapter_slug'] = isset($params['chapter_slug']) ? $params['chapter_slug'] : Str::slug($params['chapter_title']);
		$insert['chapter_content'] = $params['chapter_content']; 
		$insert['chapter_seo_title'] = isset($params['chapter_seo_title']) ? $params['chapter_seo_title'] : '';
		$insert['chapter_seo_description'] = isset($params['chapter_seo_description']) ? $params['chapter_seo_description'] : ''; 
		$insert['chapter_seo_keyword'] = isset($params['chapter_seo_keyword']) ? $params['chapter_seo_keyword'] : ''; 
		$insert['chapter_order'] = isset($params['chapter_order']) ? $params['chapter_order'] : 0; 
		
		$insert['chapter_status'] = isset($params['chapter_status']) ? $params['chapter_status'] : 'publish'; 
		$insert['language'] = isset($params['language']) ? $params['language'] : 'vi'; 
		$insert['created_by_user'] = isset($params['created_by_user']) ? $params['created_by_user'] : 1; 
		$insert['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 1; 
		$insert['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$insert['updated_at'] = date("Y-m-d H:i:s"); 
		return ChapterModel::insert($insert);		
	}
	public function update($id, $params)
	{
		$update['chapter_title'] = $params['chapter_title']; 
		$insert['story_id'] = isset($params['story_id']) ? $params['story_id'] : 0; 
		$update['chapter_slug'] = isset($params['chapter_slug']) ? $params['chapter_slug'] : Str::slug($params['chapter_title']);
		$update['chapter_content'] = $params['chapter_content']; 
		$update['chapter_seo_title'] = isset($params['chapter_seo_title']) ? $params['chapter_seo_title'] : '';
		$update['chapter_seo_description'] = isset($params['chapter_seo_description']) ? $params['chapter_seo_description'] : ''; 
		$update['chapter_seo_keyword'] = isset($params['chapter_seo_keyword']) ? $params['chapter_seo_keyword'] : ''; 
		$update['chapter_order'] = isset($params['chapter_order']) ? $params['chapter_order'] : 0; 
		$update['chapter_status'] = $params['chapter_status']; 
		$update['language'] = isset($params['language']) ? $params['language'] : 'vi';
		$update['updated_by_user'] = isset($params['updated_by_user']) ? $params['updated_by_user'] : 0; 
		$update['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s"); 
		$update['updated_at'] = date("Y-m-d H:i:s"); 
		return ChapterModel::update($id, $update);		
	}

	public function updateMany($ids, $data)
    {
        return ChapterModel::updateManyChapter($ids, $data);
	}
	
	public function deleteMany($ids)
    {
        return ChapterModel::deleteManyChapter($ids);
	}
	
	public function delete($id)
	{
		return ChapterModel::delete($id);		
	}

	public function getList(array $params)
    {
		$pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$total = self::totalRows($query);
		
		$column = ['id', 'chapter_title', 'chapter_status', 'language', 'created_at', 'chapter_order', 'chapter_slug'];
        $result = ChapterModel::getMany($column, $pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}
	
	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}

	public static function takeNew($quantity, $filter)
    {
        return ChapterModel::takeNew($quantity, $filter);
    }
}