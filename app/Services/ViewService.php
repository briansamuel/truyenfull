<?php
namespace App\Services;

use App\Models\ViewModel;
use App\Transformers\AgencyTransformer;
use Illuminate\Support\Str;

class ViewService
{
	
	
	public static function getMany($column, $pagination, $sort, $query)
	{
		$result = ViewModel::getMany($column, $pagination, $sort, $query);
        return $result ? $result : [];
	}

	public static function findByKey($key, $value)
	{
        $result = ViewModel::findByKey($key, $value);
        return $result ? $result : [];
	}
	public static function insert($params)
	{

		return ViewModel::insert($params);		
	}
	public static function update($id, $params)
	{
		$update['updated_at'] = date("Y-m-d H:i:s"); 
		return ViewModel::update($id, $update);		
	}

	public static function updateStatusCrawl($id, $params) {
		$update['crawl_status'] = $params['crawl_status']; 
		return ViewModel::update($id, $update);
	}

	public function updateMany($ids, $data)
    {
        return ViewModel::updateManyStory($ids, $data);
	}
	
	public function deleteMany($ids)
    {
        return ViewModel::deleteManyStory($ids);
	}
	
	public function delete($id)
	{
		return ViewModel::delete($id);		
	}

	
}