<?php

namespace App\Services\Auth;

use Illuminate\Http\Request;

class AuthPermissionService
{

    //CI instance
    private $CI;
    private $request;
    private $admin = 'no';
    private $is_root = 'no';
    private $requireRoot = 'no';
    //current user's permissions
    private $authobj;

    private $MyProfileController = array(
        'MyProfileController' => 'Quản lý toàn quyền(Tài Khoản => Thông tin tài khoản)',
        'MyProfileController/profile' => 'Xem thông tin cá nhân',
        'MyProfileController/updateProfile' => 'Cập nhập thông tin cá nhân',
        'MyProfileController/changePassword' => 'Thay đổi mật khẩu',
        'MyProfileController/changePasswordAction' => 'Action thay đổi mật khẩu',
    );

    private $UsersController = array(
        'UsersController' => 'Quản lý toàn quyền(Tài Khoản => Tài Khoản Hệ Thống)',
        'UsersController/index' => 'Danh sách hệ thống',
        'UsersController/ajaxGetList' => 'Action Danh sách hệ thống',
        'UsersController/add' => 'Tạo mới 1 tài khoản hệ thống',
        'UsersController/addAction' => 'Action xử lý tạo mới 1 tài khoản hệ thống',
        'UsersController/edit' => 'Sửa thông tin 1 tài khoản hệ thống',
        'UsersController/editAction' => 'Action xử lý sửa thông tin 1 tài khoản hệ thống',
        'UsersController/editManyAction' => 'Action xử lý sửa nhiều thông tin tài khoản hệ thống',
        'UsersController/delete' => 'Xóa thông tin 1 tài khoản hệ thống',
        'UsersController/deleteMany' => 'Xóa nhiều thông tin tài khoản hệ thống',
    );

    private $AgentsController = array(
        'AgentsController' => 'Quản lý toàn quyền(Tài Khoản => Tài Khoản Đại Lý)',
        'AgentsController/index' => 'Danh sách đại lý',
        'AgentsController/ajaxGetList' => 'Action Danh sách đại lý',
        'AgentsController/add' => 'Tạo mới 1 tài khoản đại lý',
        'AgentsController/addAction' => 'Action xử lý tạo mới 1 tài khoản đại lý',
        'AgentsController/edit' => 'Sửa thông tin 1 tài khoản đại lý',
        'AgentsController/editAction' => 'Action xử lý sửa thông tin 1 tài khoản đại lý',
        'AgentsController/editManyAction' => 'Action xử lý sửa nhiều thông tin tài khoản đại lý',
        'AgentsController/delete' => 'Xóa thông tin 1 tài khoản đại lý',
        'AgentsController/deleteMany' => 'Xóa nhiều thông tin tài khoản đại lý',
    );

    private $GuestsController = array(
        'GuestsController' => 'Quản lý toàn quyền(Tài Khoản => Tài Khoản Khách Hàng)',
        'GuestsController/index' => 'Danh sách khách hàng',
        'GuestsController/ajaxGetList' => 'Action Danh sách khách hàng',
        'GuestsController/add' => 'Tạo mới 1 tài khoản khách hàng',
        'GuestsController/addAction' => 'Action xử lý tạo mới 1 tài khoản khách hàng',
        'GuestsController/edit' => 'Sửa thông tin 1 tài khoản khách hàng',
        'GuestsController/editAction' => 'Action xử lý sửa thông tin 1 tài khoản khách hàng',
        'GuestsController/editManyAction' => 'Action xử lý sửa nhiều thông tin tài khoản khách hàng',
        'GuestsController/delete' => 'Xóa thông tin 1 tài khoản khách hàng',
        'GuestsController/deleteMany' => 'Xóa nhiều thông tin tài khoản khách hàng',
    );

    private $UserGroupController = array(
        'UserGroupController' => 'Quản lý toàn quyền(Tài Khoản => Quản lý phân quyền)',
        'UserGroupController/index' => 'Danh sách phân quyền',
        'UserGroupController/ajaxGetList' => 'Action Danh sách phân quyền',
        'UserGroupController/add' => 'Tạo mới 1 tài khoản phân quyền',
        'UserGroupController/addAction' => 'Action xử lý tạo mới 1 phân quyền',
        'UserGroupController/edit' => 'Sửa thông tin 1 phân quyền',
        'UserGroupController/editAction' => 'Action xử lý sửa thông tin 1 phân quyền',
        'UserGroupController/editManyAction' => 'Action xử lý sửa nhiều thông tin phân quyền',
        'UserGroupController/delete' => 'Xóa thông tin 1 phân quyền',
        'UserGroupController/deleteMany' => 'Xóa nhiều thông tin phân quyền',
    );

    private $LogsUserController = array(
        'LogsUserController' => 'Quản lý toàn quyền(Tài Khoản => Logs User)',
        'LogsUserController/index' => 'Danh sách Logs User',
        'LogsUserController/ajaxGetList' => 'Action Danh sách Logs User',
    );

    // Group quyền tin tức
    private $NewsController = array(
        'NewsController' => 'Quản lý toàn quyền (Tin tức)',
        'NewsController/index' => 'Danh sách tin tức',
        'NewsController/add' => 'Trang Thêm và Sửa tin tức',
        'NewsController/save' => 'Action Lưu dữ liệu',
        'NewsController/delete' => 'Xóa tin tức',
    );

    // Group quyền dịch vụ
    private $ServiceController = array(
        'ServiceController' => 'Quản lý toàn quyền (Dịch vụ)',
        'ServiceController/index' => 'Danh sách dịch vụ',
        'ServiceController/add' => 'Trang Thêm và Sửa tin tức',
        'ServiceController/save' => 'Action Lưu dữ liệu',
        'ServiceController/delete' => 'Xóa tin dịch vụ',
    );

    // Group quyền dự án
    private $ProjectController = array(
        'ProjectController' => 'Quản lý toàn quyền (dự án)',
        'ProjectController/index' => 'Danh sách dự án',
        'ProjectController/add' => 'Tạo mới dự án',
        'ProjectController/addAction' => 'Action Tạo mới dự án',
        'ProjectController/edit' => 'Sửa thông tin 1 dự án',
        'ProjectController/editAction' => 'Action xử lý sửa thông tin 1 dự án',
        'ProjectController/editManyAction' => 'Action xử lý sửa nhiều dự án',
        'ProjectController/delete' => 'Xóa 1 dự án',
        'ProjectController/deleteMany' => 'Xóa nhiều dự án',
    );

    // Group quyền đối tác
    private $PartnerController = array(
        'PartnerController' => 'Quản lý toàn quyền (đối tác)',
        'PartnerController/index' => 'Danh sách đối tác',
        'PartnerController/add' => 'Tạo mới đối tác',
        'PartnerController/addAction' => 'Action Tạo mới đối tác',
        'PartnerController/edit' => 'Sửa thông tin 1 đối tác',
        'PartnerController/editAction' => 'Action xử lý sửa thông tin 1 đối tác',
        'PartnerController/editManyAction' => 'Action xử lý sửa nhiều đối tác',
        'PartnerController/delete' => 'Xóa 1 đối tác',
        'PartnerController/deleteMany' => 'Xóa nhiều đối tác',
    );

    // Group quyền khách sạn
    private $HostController = array(
        'HostController' => 'Quản lý toàn quyền (Khách sạn)',
        'HostController/index' => 'Danh sách khách sạn',
        'HostController/add' => 'Tạo mới khách sạn',
        'HostController/addAction' => 'Action Tạo mới khách sạn',
        'HostController/edit' => 'Sửa thông tin 1 khách sạn',
        'HostController/editAction' => 'Action xử lý sửa thông tin 1 khách sạn',
        'HostController/editManyAction' => 'Action xử lý sửa nhiều khách sạn',
        'HostController/delete' => 'Xóa 1 khách sạn',
        'HostController/deleteMany' => 'Xóa nhiều khách sạn',
    );

    // Group quyền phòng
    private $RoomController = array(
        'RoomController' => 'Quản lý toàn quyền (Phòng)',
        'RoomController/index' => 'Danh sách phòng',
        'RoomController/add' => 'Tạo mới phòng',
        'RoomController/addAction' => 'Action Tạo mới phòng',
        'RoomController/edit' => 'Sửa thông tin 1 phòng',
        'RoomController/editAction' => 'Action xử lý sửa thông tin 1 phòng',
        'RoomController/editManyAction' => 'Action xử lý sửa nhiều phòng',
        'RoomController/delete' => 'Xóa 1 phòng',
        'RoomController/deleteMany' => 'Xóa nhiều phòng',
    );

    // Group quyền bình luận
    private $CommentController = array(
        'CommentController' => 'Quản lý toàn quyền (Bình luận)',
        'CommentController/index' => 'Danh sách bình luận',
        'CommentController/add' => 'Tạo mới bình luận',
        'CommentController/addAction' => 'Action Tạo mới bình luận',
        'CommentController/edit' => 'Sửa thông tin 1 bình luận',
        'CommentController/editAction' => 'Action xử lý sửa thông tin 1 bình luận',
        'CommentController/editManyAction' => 'Action xử lý sửa nhiều bình luận',
        'CommentController/delete' => 'Xóa 1 bình luận',
        'CommentController/deleteMany' => 'Xóa nhiều bình luận',
    );

    // Group quyền đánh giá
    private $ReviewController = array(
        'ReviewController' => 'Quản lý toàn quyền (Đánh giá)',
        'ReviewController/index' => 'Danh sách đánh giá',
        'ReviewController/add' => 'Tạo mới đánh giá',
        'ReviewController/addAction' => 'Action Tạo mới đánh giá',
        'ReviewController/edit' => 'Sửa thông tin 1 đánh giá',
        'ReviewController/editAction' => 'Action xử lý sửa thông tin 1 đánh giá',
        'ReviewController/editManyAction' => 'Action xử lý sửa nhiều đánh giá',
        'ReviewController/delete' => 'Xóa 1 đánh giá',
        'ReviewController/deleteMany' => 'Xóa nhiều đánh giá',
    );

    // Group thư viện
    private $GalleryController = array(
        'GalleryController' => 'Quản lý toàn quyền (Thư viện)',
        'GalleryController/index' => 'Quản lý thư viện',
    );

    private $MenusController = array(
        'MenusController' => 'Quản lý toàn quyền (Menu)',
        'MenusController/index' => 'Quản lý Menu',
    );

    private $CustomCssController = array(
        'CustomCssController' => 'Quản lý toàn quyền (Custom Css)',
        'CustomCssController/index' => 'Tùy biến Css',
        'CustomCssController/editAction' => 'Action Tùy biến Css',
    );

    private $ThemeOptionsController = array(
        'ThemeOptionsController' => 'Quản lý toàn quyền (Tùy biến)',
        'ThemeOptionsController/index' => 'Tùy biến',
        'ThemeOptionsController/editAction' => 'Action Tùy biến',
    );
    /*
     * construct
     */

    public function __construct(Request $request, $requireRoot = false)
    {
        $this->CI = new \stdClass();

        $this->request = $request;
        $this->requireRoot = $requireRoot;
        $this->is_root = $request->session()->get('is_root');
        
        //current permission
        $this->authobj = json_decode($request->session()->get('permission'));
        $this->authobj = (!is_array($this->authobj)) ? array() : $this->authobj;
    }

    function check()
    {
        //is root
        if ($this->is_root == 1) {
            
            return true;
        }
        //only root
        if ($this->requireRoot && $this->is_root != 1) {
            return false;
        }
        //lấy controller - function hiện tại qua router
        $currentAction = \Route::currentRouteAction();
        list($controllers, $method) = explode('@', $currentAction);
        // $controller now is "App\Http\Controllers\FooBarController"
        $controller = preg_replace('/.*\\\/', '', $controllers);
        if($controller === 'WelcomeController') {
            return true;
        }
        //tên controller/function
        $function = $controller . '/' . $method;
        //full access controller
        if (in_array($controller, $this->authobj)) {
            return true;
        }
        //can access
        if (in_array($function, $this->authobj)) { //|| in_array($method, $this->ignores)) {
            return true;
        }
        //no permission
        return false;
    }

    /*
     * get list controller
     */

    function listController()
    {
        $this->controllers = array(
            'MyProfileController' => $this->MyProfileController,
            'UsersController' => $this->UsersController,
            'AgentsController' => $this->AgentsController,
            'GuestsController' => $this->GuestsController,
            'UserGroupController' => $this->UserGroupController,
            'LogsUserController' => $this->LogsUserController,
            'NewsController' => $this->NewsController,
            'ServiceController' => $this->ServiceController,
            'ProjectController' => $this->ProjectController,
            'PartnerController' => $this->PartnerController,
            'HostController' => $this->HostController,
            'RoomController' => $this->RoomController,
            'CommentController' => $this->CommentController,
            'ReviewController' => $this->ReviewController,
            'MenusController' => $this->MenusController,
            'GalleryController' => $this->GalleryController,
            'ThemeOptionsController' => $this->ThemeOptionsController,
            'CustomCssController' => $this->CustomCssController,
        );
        return $this->controllers;
    }

    /*
     * return admin
     */

    public function isAdmin()
    {
        if ($this->admin == 'yes') {
            return true;
        } else {
            return false;
        }
    }

    /*
     * check function show in header
     */

    function checkHeader($controller)
    {
        if ($this->is_root == 1) {
            return true;
        }
        foreach ($this->authobj as $permission) {
            if (strpos($controller, $permission) === 0) {
                return true;
            }
        }
        return false;
    }

    public function isHeader($controller)
    {
        if ($this->is_root == 1) {
            return true;
        }
        if (in_array($controller, $this->authobj)) {
            return true;
        } else {
            return false;
        }
    }

    public function getListPermission($encode = true)
    {
        $permissions = [];
        foreach ($this->listController() as $key => $value) {
            //neu co 1 chuc nang dc lua chon
            if ($this->request->input($key, '')) {
                $permission = $this->request->input($key, '');
                //neu la toan quyen
                if ($permission[0] == $key) {
                    $permissions[] = $key;
                } else {
                    foreach ($permission as $x) {
                        $permissions[] = $x;
                    }
                }
            }
        }

        return $encode ? json_encode($permissions) : $permissions;
    }

}

?>
