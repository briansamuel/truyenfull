<?php

namespace App\Services;

use App\Models\ReviewModel;
use App\Transformers\ContactTransformer;

class ReviewService
{

    public static function totalRows() {
        $result = ReviewModel::totalRows();
        return $result;
    }

    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return ReviewModel::add($params);
    }

    public function edit($id, $params)
    {
        $params['updated_at'] = date("Y-m-d H:i:s");
        return ReviewModel::update($id, $params);
    }

    public function deleteMany($ids)
    {
        return ReviewModel::deleteMany($ids);
    }

    public function updateMany($ids, $data)
    {
        return ReviewModel::updateMany($ids, $data);
    }

    public function delete($ids)
    {
        return ReviewModel::delete($ids);
    }

    public function detail($id)
    {
        $detail = ReviewModel::findById($id);
        $host = HostService::findByKey('id', $detail->host_id);
        if($host) {
            $detail->host_name = $host->host_name;
        } else {
            $detail->host_name = 'Không tìm thấy khách sạn';
        }
        return ContactTransformer::transformItem($detail);
    }

    public function getList(array $params)
    {
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$total = self::totalRows($query);

        $result = ReviewModel::getMany($pagination, $sort, $query);
        foreach($result as $review) {
            $host = HostService::findByKey('id', $review->host_id);
            if($host) {
                $review->host_name = $host->host_name;
            } else {
                $review->host_name = 'Không tìm thấy khách sạn';
            }
        }
        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public function getListIDs($data) {

        $ids = array();

        foreach($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

    public static function takeNew($quantity)
    {
        return ReviewModel::takeNew($quantity);
    }

}
