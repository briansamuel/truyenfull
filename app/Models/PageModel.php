<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class PageModel
{
    //
    protected static $table = 'pages';

    
    public static function getMany($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('page_status', '=', $filter['status']);
        }
        if(isset($filter['page_title']) && $filter['page_title'] != ""){
            $query->where('page_title', 'like', "%".$filter['page_title']."%");
        }

        if(isset($filter['page_type']) && $filter['page_type'] != ""){
            $query->where('page_type', '=', $filter['page_type']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }

        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('page_status', '=', $filter['status']);
        }
        if(isset($filter['page_title']) && $filter['page_title'] != ""){
            $query->where('page_title', 'like', "%".$filter['page_title']."%");
        }

        if(isset($filter['page_type']) && $filter['page_type'] != ""){
            $query->where('page_type', '=', $filter['page_type']);
        }
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        return $query->count();

    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateManyPage($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyPage($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }
}
