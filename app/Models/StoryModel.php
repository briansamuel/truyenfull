<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class StoryModel
{
    //
    protected static $table = 'stories';

    
    public static function getMany($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $pagination['page']  = isset($pagination['page']) ? $pagination['page'] : 1;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);
        
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('story_status', '=', $filter['status']);
        }
        if(isset($filter['story_title']) && $filter['story_title'] != ""){
            $query->where('story_title', 'like', "%".$filter['story_title']."%");
        }

        if(isset($filter['story_source']) && $filter['story_source'] != ""){
            $query->where('story_source', 'like', "%".$filter['story_source']."%");
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        } else {
            $query->orderBy('id', 'DESC');
        }

        return $query->get();
    }

    public static function getManyWithPaginate($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $pagination['page']  = isset($pagination['page']) ? $pagination['page'] : 1;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns);
        
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('story_status', '=', $filter['status']);
        }
        if(isset($filter['story_title']) && $filter['story_title'] != ""){
            $query->where('story_title', 'like', "%".$filter['story_title']."%");
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }

        return $query->paginate($pagination['perpage']);
    }

    public static function getManyByCategory($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $pagination['page']  = isset($pagination['page']) ? $pagination['page'] : 1;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)
        ->leftJoin('category_story_relations', self::$table.'.id', '=', 'category_story_relations.story_id')
        ->leftJoin('chapters', self::$table.'.id', '=', 'chapters.story_id')
        ->select($columns)->skip($offset)->take($pagination['perpage']);
        
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_story_relations.category_id', '=', $filter['category_id']);
        }
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where(self::$table.'story_status', '=', $filter['status']);
        }

        if(isset($filter['story_title']) && $filter['story_title'] != ""){
            $query->where(self::$table.'story_title', 'like', "%".$filter['story_title']."%");
        }

        if (isset($filter[self::$table.'created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where(self::$table.'created_at', '>=', $start_time);
            $query->where(self::$table.'created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy(self::$table.'.'.$sort['field'], $sort['sort']);
        }

        return $query->groupBy('chapters.story_id')->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('story_status', '=', $filter['status']);
        }
        if(isset($filter['story_title']) && $filter['story_title'] != ""){
            $query->where('story_title', 'like', "%".$filter['story_title']."%");
        }


        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        return $query->count();

    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateManyStory($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyStory($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function takeNew($quantity, $filter)
    {
        $query = DB::table(self::$table);
        
        
        return $query->orderBy('id', 'DESC')->limit($quantity)->get();
    }
}
