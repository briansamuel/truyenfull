<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class UserGroupModel
{
    private static $table = 'user_groups';

    public static function getAll()
    {
        $result = DB::table(self::$table)->get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter = [])
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function getGroupActive()
    {
        $result = DB::table(self::$table)->where('enabled', 1)->get();
        return $result ? $result : [];
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findExistByKey($key, $value, $id = '')
    {
        if($id === '') {
            $result = DB::table(self::$table)->where($key, $value)->first();
        } else {
            $result = DB::table(self::$table)->where($key, $value)->where('id', '!=', $id)->first();
        }
        return $result;
    }

    public static function getById($ids)
    {
        $result = DB::table(self::$table)->whereIn('id', $ids)->get();
        return $result ? $result : [];
    }

    public static function totalRows() {
        $result = DB::table(self::$table)->count();
        return $result;
    }

    public static function add($data) {
        return DB::table(self::$table)->insertGetId($data);
    }

    public static function edit($id, $data) {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();
    }

    public static function updateMany($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function deleteMany($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }
}
