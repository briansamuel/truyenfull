<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class CategoryStoryModel
{
    //
    protected static $table = 'category_story_relations';


    public static function getAllByKey($columns = ['*'], $filter, $array = false)
    {
        $query = DB::table(self::$table)->select($columns);
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }
        
        if(isset($filter['story_ids']) && $filter['story_ids'] != ""){
            $query->whereIn('story_id', $filter['story_ids']);
        }

        if($array) {
            return $query;
        } else {
            return $query->get();
        }
        
    }

    public static function getMany($columns = ['*'], $pagination, $sort, $filter, $array = false)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $pagination['page']  = isset($pagination['page']) ? $pagination['page'] : 1;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }
        
        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }
        
       

        return $query->get();

        if($array) {
            return $query;
        } else {
            return $query->get();
        }
        
    }

    

    public static function totalRows($filter) {
        $query = DB::table(self::$table);

        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }
    
        $result = $query->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function deleteManyByKey($filter)
    {   
        $query = DB::table(self::$table);
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', '=', $filter['category_id']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }
        
        return $query->delete();
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }
}
