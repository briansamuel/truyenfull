<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ChapterModel
{
    //
    protected static $table = 'chapters';

    
    public static function getMany($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $pagination['page']  = isset($pagination['page']) ? $pagination['page'] : 1;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('chapter_status', '=', $filter['status']);
        }
        if(isset($filter['chapter_title']) && $filter['chapter_title'] != ""){
            $query->where('chapter_title', 'like', "%".$filter['chapter_title']."%");
        }

        if(isset($filter['chapter_slug']) && $filter['chapter_slug'] != ""){
            $query->where('chapter_slug', '=', $filter['chapter_slug']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }

        if(isset($filter['story_ids']) && $filter['story_ids'] != ""){
            $query->whereIn('story_id', $filter['story_ids']);
        }

        if(isset($filter['ids']) && $filter['ids'] != ""){
            $query->whereIn('id', $filter['ids']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }
        
       

        return $query->get();
    }

    public static function getLastestChapter($columns = ['*'],$pagination, $sort, $filter){
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $pagination['page']  = isset($pagination['page']) ? $pagination['page'] : 1;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table);
        
        $query->whereIn('id', array(DB::raw("select max(`id`) from shtbl_chapters GROUP BY story_id")) );

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }
        
        $query->select($columns)->skip($offset)->take($pagination['perpage']);

        return $query->get();
    }

    public static function getLastestChapterPaginate($columns = ['*'],$pagination, $sort, $filter){
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $pagination['page']  = isset($pagination['page']) ? $pagination['page'] : 1;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table);
        
        $query->whereIn('id', array(DB::raw("select max(`id`) from shtbl_chapters GROUP BY story_id")) );

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }
        
        $query->select($columns);

        return $query->paginate($pagination['perpage']);
    }

    public static function getChapters($columns = ['*'],$pagination, $sort, $filter, $group)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('chapter_status', '=', $filter['status']);
        }
        if(isset($filter['chapter_title']) && $filter['chapter_title'] != ""){
            $query->where('chapter_title', 'like', "%".$filter['chapter_title']."%");
        }

        if(isset($filter['chapter_slug']) && $filter['chapter_slug'] != ""){
            $query->where('chapter_slug', '=', $filter['chapter_slug']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }

        
        
        if($group['field'] && $group['field'] != "") {
            $query->groupBy($group['field']);
            
        }

        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }

        $query->latest();
        $query->groupBy('story_id');
        
        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('chapter_status', '=', $filter['status']);
        }

        if(isset($filter['chapter_title']) && $filter['chapter_title'] != ""){
            $query->where('chapter_title', 'like', "%".$filter['chapter_title']."%");
        }

        if(isset($filter['chapter_slug']) && $filter['chapter_slug'] != ""){
            $query->where('chapter_slug', '=', $filter['chapter_slug']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }

        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        return $query->count();

    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }

    public static function findByKeyLast($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->orderBy('created_at', 'DESC' )->first();
        return $data ? $data : [];
    }

    public static function findByParams($columns = ['*'], $filter)
    {

        $query = DB::table(self::$table)->select($columns);

        if(isset($filter['chapter_title']) && $filter['chapter_title'] != ""){
            $query->where('chapter_title', 'like', "%".$filter['chapter_title']."%");
        }

        if(isset($filter['chapter_slug']) && $filter['chapter_slug'] != ""){
            $query->where('chapter_slug', '=', $filter['chapter_slug']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }
        $data = $query->first();
        return $data ? $data : [];
    }

    public static function nextChapter($columns = ['*'], $filter)
    {

        $query = DB::table(self::$table)->select($columns);

    

        if(isset($filter['id']) && $filter['id'] != ""){
            $query->where('id', '>', $filter['id']);
        }

        if(isset($filter['chapter_order']) && $filter['chapter_order'] != ""){
            $query->where('chapter_order', '>', $filter['chapter_order']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }
        $data = $query->orderBy('id','ASC')->first();
        return $data ? $data : [];
    }

    public static function previousChapter($columns = ['*'], $filter)
    {

        $query = DB::table(self::$table)->select($columns);

        if(isset($filter['id']) && $filter['id'] != ""){
            $query->where('id', '<', $filter['id']);
        }

        if(isset($filter['chapter_order']) && $filter['chapter_order'] != ""){
            $query->where('chapter_order', '<', $filter['chapter_order']);
        }

        if(isset($filter['story_id']) && $filter['story_id'] != ""){
            $query->where('story_id', '=', $filter['story_id']);
        }
        $data = $query->orderBy('id','DESC')->first();
        return $data ? $data : [];
    }

    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateManyChapter($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyChapter($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function takeNew($quantity, $filter)
    {
        $query = DB::table(self::$table);
        
        
        return $query->orderBy('id', 'DESC')->limit($quantity)->get();
    }
}
