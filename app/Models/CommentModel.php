<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class CommentModel
{

    protected static $table = 'comments';

    public static function getAll()
    {
        $result = DB::table(self::$table)::get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['name_guest']) && $filter['name_guest'] != ""){
            $query->where('name_guest', 'like', '%'.$filter['name_guest'].'%');
        }

        if(isset($filter['comment_status']) && $filter['comment_status'] != ""){
            $query->where('comment_status', '=', $filter['comment_status']);
        }
        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', '=', $filter['language']);
        }

        if(isset($filter['post_id']) && $filter['post_id'] != ""){
            $query->where('post_id', $filter['post_id']);
        }
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows() {
        $result = DB::table(self::$table)->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function update($id, $data)
    {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }

    public static function deleteMany($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function updateMany($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();
    }

    public static function add($data)
    {
        return DB::table(self::$table)->insert($data);
    }

    public static function takeNew($quantity)
    {
        return DB::table(self::$table)->orderBy('id', 'DESC')->limit($quantity)->get();
    }
}
