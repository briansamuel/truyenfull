<?php

namespace App\Http\Controllers\Crawl;

use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;
use App\Services\CategoryService;
use App\Services\CategoryStoryService;
use App\Services\StoryService;
use App\Services\ChapterService;
use App\Services\ValidationService;
use App\Services\LogsUserService;
use Exception;
use HTMLDomParser;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Helpers\Common;
class CrawlTruyenCVController extends Controller
{

    public $failCount = 0;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */
    public function crawl()
    {   
        $sourceTruyen = Common::sourceTruyen();
        return view('admin.crawlers.index', compact('sourceTruyen'));
    }

    public function crawlChapter()
    {   
        $sourceTruyen = Common::sourceTruyen();
        return view('admin.crawlers.chapter', compact('sourceTruyen'));
    }

    public function index()
    {
        // $url = "https://truyencv.com/tien-hiep/";
        // $html = $this->curl($url);
        // $stories_link = HTMLDomParser::str_get_html($html)->find('h2.title a');
        // foreach ($stories_link as $story) {
        //     //echo $story->href.'<br>';

        //     $row = StoryService::findByKey('story_slug', Str::slug($story->plaintext));
        //     if ($row) {
        //         $this->fillGenres($row->id, $story->href);
        //         echo 'Truyện ' . $story->plaintext . ' đã có sẵn <br>';
        //         //$this->listChapter($row->id, $story->href);
        //     } else {
        //         $data = $this->fillStory($story->href);

        //         if ($data) {
        //             $insert_id = StoryService::insert($data);
        //             if ($story) {
        //                 $this->fillGenres($insert_id, $story->href);
        //                 echo 'Truyện ' . $story->plaintext . ' thêm thành công <br>';
        //             } else {
        //                 echo 'Truyện ' . $story->plaintext . ' thêm thất bại <br>';
        //             }
        //         }
        //     }
        // }
        // $url ="https://truyenfull.net/";
        // $html = $this->curl($url);
        // $categories = HTMLDomParser::str_get_html($html)->find('.col-truyen-side .list-cat a');

        // foreach($categories as $category) {

        //     //echo $story->href.'<br>';

        //     $row = CategoryService::findByKey('category_name', $category->plaintext);
        //     if($row) {
        //         echo 'Thể loại '.$category->plaintext.' đã có sẵn <br>';
        //     } else {
        //         $data = $this->fillCategories($category->href);
        //         if($data) {
        //             $insert = CategoryService::insert($data);
        //             if($insert) {
        //                 echo 'Thể loại '.$category->plaintext.' thêm thành công <br>';
        //             } else {
        //                 echo 'Thể loại '.$category->plaintext.' thêm thất bại <br>';
        //             }
        //         }


        //     }
        // }
        $array_truyen = [];
        for($i = 1; $i < 37; $i++) {
            $array_truyen[] = 'https://truyencv.com/kiem-hiep/trang-'.$i.'/';

        }

        for($i = 1; $i < 206; $i++) {
            $array_truyen[] = 'https://truyencv.com/do-thi/trang-'.$i.'/';
           
        }

        $new_args_truyen = array_reverse($array_truyen, false );
        foreach($new_args_truyen as $k => $i) {
            echo $i.'<br>';
        }
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_HEADER, true);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $stories = StoryService::getMany(['id','story_slug','story_source'], ['page' => 1,'perpage' => 1000], ['field' => 'id', 'sort' => 'ASC'], ['story_source' => 'TruyenCV']);
        // foreach($stories as $key => $story) {
        //     $url = 'https://truyencv.com/'.$story->story_slug.'/';
        //     curl_setopt($ch, CURLOPT_URL, $url);
        //     $out = curl_exec($ch);

        //     // line endings is the wonkiest piece of this whole thing
        //     $out = str_replace("\r", "", $out);

        //     // only look at the headers
        //     $headers_end = strpos($out, "\n\n");
        //     if( $headers_end !== false ) { 
        //         $out = substr($out, 0, $headers_end);
        //     }   

        //     $headers = explode("\n", $out);
        //     foreach($headers as $header) {
        //         $str =  substr($header, 0, 10);
        //         if( $str == "location: " ) { 
        //             $target = substr($header, 10);
        //             $parse = parse_url($target);
        //             $domain = $parse['host'];
        //             StoryService::updateOnly($story->id, ['story_source' => $domain]);
        //             echo "[$url] redirects to [$domain]<br>";
        //             continue 2;
        //         }   
        //     }   
        //     StoryService::updateOnly($story->id, ['story_source' => 'truyencv.com']);
        //     echo "[$url] does not redirect<br>";
        // }


        // die();
    }

    public function listPage()
    {
        for ($i = 1; $i < 205; $i++) {
            echo 'https://truyencv.com/xuyen-khong/trang-' . $i . '/';
            echo '<br>';
        }

        
    }

    public function ajaxListStory()
    {
        $params = $this->request->only('page');
        $data = array();
        $data['list_story'] = array();
        if (isset($params['page'])) {
            $url = $params['page'];

            $html = $this->curl($url);

            if ($html) {



                $stories_link = HTMLDomParser::str_get_html($html)->find('.list-group-item-table h2.title a');
                foreach ($stories_link as $story) {
                    //echo $story->href.'<br>';

                    array_push($data['list_story'], $story->href);
                }

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách truyện thành công</strong>';
            } else {
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm HTML</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }
        //echo 'TEST';
        echo json_encode($data);
        die();
    }



    public function ajaxListChapter()
    {   
        
        $params = $this->request->only('url');
        $data = array();
        $data['list_chapter'] = array();
        $story_status = '';
        if (isset($params['url'])) {
            
            $story = $this->fillStory($params['url']);
            $row = StoryService::findByKey('story_title', $story['story_title']);
            if ($row) {
                $genres = $this->fillGenres($row->id, $params['url']);
                $story_status = 'Truyện ' . $story['story_title'] . ' đã có sẵn ' . $genres;
                $data['message'] = '<strong class="kt-font-warning kt-font-bold">' . $story_status . '</strong>';
            } else {


                if ($story) {
                   
                    if ($story) {
                        $image_url = $story['story_thumbnail'];
                        if ($image_url) {
                            $month = date('m');
                            $year = date('Y');
                            $destinationPath = 'uploads/files/'.$year.'/'.$month.'/';
                            if (!is_dir('./' . $destinationPath)) {
                                    mkdir('./' . $destinationPath, 0777, true);
                            }
                
                        $image = $this->grab_image($image_url, $destinationPath . $story['story_slug'] . '.jpg');
                            $story['story_thumbnail'] = $destinationPath . $story['story_slug'] . '.jpg';
                        }
                        $insert_id = StoryService::insert($story);
                        $genres = $this->fillGenres($insert_id, $params['url']);
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thành công ' . $genres;
                        $data['message'] = '<strong class="kt-font-success kt-font-bold">' . $story_status . '</strong>';
                    } else {
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thất bại ';
                        $data['message'] = '<strong class="kt-font-danger kt-font-bold">' . $story_status . '</strong>';
                    }
                } else {
                    $story_status = 'Truyện ' . $story['story_title'] . ' dữ liệu bị lỗi, vui lòng thử lại ';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">' . $story_status . '</strong>';
                }
            }

            // try {

            $data['status']    = 'success';
            //     $this->mergeListChapter($data['list_chapter'], $params['url']);
            //     $count = count($data['list_chapter']);
            //     $data['status'] = 'success';
            //     $data['message'] = '<strong class="kt-font-success kt-font-bold">' . $story_status . ' - Đã lấy ' . $count . ' chapter thành công</strong>';
            // } catch (Exception $e) {
            //     $data['status'] = 'error';
            //     $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, vui lòng thử lại</strong>';
            // }

        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }

        echo json_encode($data);
        die();
    }

    // TruyệnCV
    public function ajaxListChapter2()
    {
        $params = $this->request->only('ids');

        $ids = $params['ids'];
        
        
        $ids = array_unique($ids);
        $data = [];
        $data['data'] = [];
        $array_chapter = [];
        $title = '';
        foreach ($ids as $id) {
            $array_chapter = array();
            $row = StoryService::findByKey('id', $id);
            $url = 'https://truyencv.com/' . $row->story_slug . '/';
            try {
                $html = $this->curl($url);
                if(isset($row)) {
                    
                    if($row->crawl_status == 'done') {
                        $data['status'] = 'error';
                        $data['message'] = '<strong class="kt-font-danger kt-font-bold">Truyện này đã lấy chương</strong>';
                    } else {
                        if ($html) {
                            try {
                                
                                $link_id = isset(HTMLDomParser::str_get_html($html)->find('.list-popover li a')[0]) ? HTMLDomParser::str_get_html($html)->find('.list-popover li a')[0]->href : null;
                                if($link_id) {
                                    $list_id = explode('/', $link_id);
                                    $id_truyencv = isset($list_id[3]) ? (int) $list_id[3] : 1;
            
                                    $para = 'showChapter=1&media_id=' . $id_truyencv . '&number=1&page=1&type=' . $row->story_slug;
            
                                    
                                    $dataChapter = $this->curlPost('https://truyencv.com/index.php', $para);
                                    
                                    $pattern = '~[a-z]+://\S+~';
                                    $links = [];
                                    preg_match_all($pattern, $dataChapter, $links);
                                    
                                    if(isset($links[0])) {
                                        
                                        // $list_chapter = $domChapter->find('.item a');
                                        // $list_chapter = $domChapter->find('.item a');
                                        $array_chapter = array_reverse($links[0], false);
                                        
                                        // foreach ($list_chapter as $chapter) {
                
                                        //     array_push($array_chapter, $chapter->href);
                                        // }
                                    } else {
                                        $data['status'] = 'error';
                                        $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, không lấy được HTML</strong>';
                                    }
                                    
                                }
                                
                            
                                $data['status'] = 'success';
                                $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách chương thành công</strong>';
                            } catch(Exception $e) {
                                $data['status'] = 'error';
                                $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách chương thành công</strong>';
                            }
                            
            
                        
                        } else {
                            $data['status'] = 'error';
                            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, không lấy được HTML</strong>';
                        }
                    }
                    $title = $row->story_title;
                }
                

            } catch(Exception $e){
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi :'.$e->getMessage().'</strong>';
            }
           
            
            array_push($data['data'],array('story_id' => $id, 'story_title' => $title ,'list_chapter' => $array_chapter));
            
        }

        
        echo json_encode($data);
        die();
    }

    public function ajaxUpdateCrawlLogs() {

        try {
            $params = $this->request->only('id');

            $id = $params['id'];
            $params['crawl_status'] = 'done';
            $update_id = StoryService::updateStatusCrawl($id, null);
            if($update_id) {

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Cập nhật crawl Logs thành công</strong>';
            }
        } catch(Exception $e) {
            
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-success kt-font-bold">Cập nhật crawl Logs thất bại</strong>';
        }
        
        echo json_encode($data);
        die();
    }
    public function ajaxAddChapter()
    {
        $params = $this->request->only('id','url');
        //dd($params);
        $data = $this->fillChapter(1, $params['url']);
        if ($data) {

            
            $filter = array('story_id' => $params['id'], 'chapter_slug' => $data['chapter_slug']);
            //dd($filter);
            $chapter = ChapterService::totalRows($filter);
            if ($chapter < 1) {
                $data['story_id'] = $params['id'];
                $insert_id = ChapterService::insert($data);
                 
                if($insert_id) {
                    StoryService::updateOnly($params['id'], null);
                    $data['status'] = 'sucess';
                    $data['message'] = '<strong class="kt-font-success kt-font-bold">'.$data['chapter_title'].' : Đã lấy thành công  ('.$this->failCount.' lần lỗi)</strong>';
                } else {
                    $data['status'] = 'sucess';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">'.$data['chapter_title'].' : Lấy thất bại</strong>';
                }
            } else {
                $data['status'] = 'sucess';
                $data['message'] = '<strong class="kt-font-warning kt-font-bold">'.$data['chapter_title'].' : Đã có sẵn</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold"> Lỗi khi lấy dữ liệu ('.$this->failCount.' lần)</strong>';
        }
        $this->failCount = 0;
        echo json_encode($data);
        die();
    }

    public function mergeListChapter(&$list_chapter, $url)
    {

        $html = $this->curl($url);

        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');

            foreach ($chapters as $chapter) {
                array_push($list_chapter, $chapter->href);
            }

            $pagination = isset(HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href) ? HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href : false;
            if ($pagination) {
                self::mergeListChapter($list_chapter, $pagination);
            }
            return true;
        }
        return false;
    }

    public function fillStory($url)
    {

        try {
            $html = $this->curl($url);
            $params = array();
            if ($html) {
                $params['story_title'] = isset(HTMLDomParser::str_get_html($html)->find('h1.title')[0]) ? HTMLDomParser::str_get_html($html)->find('h1.title')[0]->plaintext : null;
                if ($params['story_title'] == null) {
                    return false;
                } else {
                    $params['story_title'] = trim($params['story_title']);
                }
                $params['story_slug'] = Str::slug($params['story_title']);
                $params['story_description'] = HTMLDomParser::str_get_html($html)->find('.brief')[0]->innertext;
                $params['story_seo_title'] =  $params['story_title'];
                $params['story_seo_keyword'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content : $params['story_title'];
                $params['story_seo_description'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content : $params['story_title'];
                $params['story_author'] = HTMLDomParser::str_get_html($html)->find('a.author')[0]->plaintext;
                $params['story_source'] = 'truyencv.com';
                $params['story_status'] = isset(HTMLDomParser::str_get_html($html)->find('.item .item-value')[2]) ? HTMLDomParser::str_get_html($html)->find('.item .item-value')[2]->plaintext : 'coming';

                $params['story_status'] =  trim(str_replace('Hoàn thành', 'full', $params['story_status']));
                $params['story_status'] =  trim(str_replace('Đang ra', 'coming', $params['story_status']));
                $params['story_status'] =  trim(str_replace('Tạm dừng', 'coming', $params['story_status']));
                $params['story_status'] =  trim(str_replace('Dừng viết', 'coming', $params['story_status']));
                $image_url = HTMLDomParser::str_get_html($html)->find('.col-thumb .img-responsive');
                $params['story_thumbnail'] = isset($image_url[0]) ? $image_url[0]->src : '';


                $params['created_by_user'] =  1;
                $params['updated_by_user'] =  1;
                return $params;
            }

           
        } catch(Exception $e) {

          
        }
        
        return false;
    }
    public function fillGenres($id, $url)
    {
        $html = $this->curl($url);
        $params = array();
        $genres_text = ' | Thể loại : ';
        if ($html) {
            $genres = HTMLDomParser::str_get_html($html)->find('.categories a');
            foreach ($genres as $genre) {
                if ($genre->plaintext) {
                    $title = trim($genre->plaintext);
                    $row = CategoryService::findByKey('category_name', trim($title));
                    if ($row) {
                        $params = array('category_id' => $row->id, 'story_id' => $id);
                        $count = CategoryStoryService::totalRows($params);
                        if ($count < 1) {
                            CategoryStoryService::insert($params);
                        }
                        $genres_text .= $title . ', ';
                    }
                }
            }
        }
        return $genres_text;
    }
    public function fillCategories($url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $params['category_name'] = HTMLDomParser::str_get_html($html)->find('span[itemprop="title"]')[1]->plaintext;
            $params['category_description'] = HTMLDomParser::str_get_html($html)->find('.cat-desc .panel-body')[0]->innertext;
            $params['category_seo_title'] = HTMLDomParser::str_get_html($html)->find('title')[0]->plaintext;
            $params['category_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
            $params['category_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;

            return $params;
        }
        return false;
    }

    public function fillChapter($order, $url)
    {
        try {   
            $html = $this->curl($url);

            $params = array();
            if ($html) {

               
                //dd($html);
                $params['chapter_title'] = HTMLDomParser::str_get_html($html)->find('h2.title')[0]->innertext;
                $params['chapter_slug'] = explode('/', $url)[4];
                $params['chapter_content'] = HTMLDomParser::str_get_html($html)->find('#js-truyencv-content')[0]->innertext;
                $params['chapter_seo_title'] =  $params['chapter_title'];
                $params['chapter_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
                $params['chapter_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
                $order = str_replace('chuong-', '', $params['chapter_slug']);
                $order = str_replace('quyen-', '', $order);
                $order = str_replace('-', '', $order);
                $order = !empty($order) ? (int) $order : 1;
                $params['chapter_order'] = $order;

                $params['created_by_user'] =  1;
                $params['updated_by_user'] =  1;
                //dd($params);
                return $params;
            } else {
                if($this->failCount < 3) {
                    $this->failCount = $this->failCount++;
                    $this->fillChapter(1, $url);
                } else {
                    $this->failCount = 0;
                    return false;
                }
            }

            
        } catch(Exception $e) {
            if($this->failCount < 3) {
                $this->failCount = $this->failCount++;
                $this->fillChapter(1, $url);
            } else {
                $this->failCount = 0;
                return false;
            }
            
           
        }

       
        
    }
    public function listChapter($story_id, $url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');
            $order = 1;
            foreach ($chapters as $chapter) {
                if ($chapter->href) {
                    $data = $this->fillChapter($order, $chapter->href);
                    if ($data) {
                        $data['story_id'] = $story_id;
                        $params = array('story_id' => $story_id, 'chapter_slug' => $data['chapter_slug']);
                        $count = ChapterService::totalRows($params);
                        if ($count < 1) {
                            ChapterService::insert($data);
                        }
                    }
                }
                $order++;
            }

            $pagination = isset(HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href) ? HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href : false;
            if ($pagination) {
                self::listChapter($story_id, $pagination);
            }
            return true;
        }
        return false;
    }

    public function curl($url)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 360);
        curl_setopt($ch, CURLOPT_TIMEOUT, 360); //timeout in seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true); // some output will go to stderr / error_log
        curl_setopt($ch, CURLOPT_REFERER, 'https://truyencv.com/');
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public function curlPost($url, $para)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 360);
        curl_setopt($ch, CURLOPT_TIMEOUT, 360); //timeout in seconds
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $para);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function grab_image($url, $saveto)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }

    function checkSource($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $out = curl_exec($ch);

        // line endings is the wonkiest piece of this whole thing
        $out = str_replace("\r", "", $out);

        // only look at the headers
        $headers_end = strpos($out, "\n\n");
        if( $headers_end !== false ) { 
            $out = substr($out, 0, $headers_end);
        }   

        $headers = explode("\n", $out);
        foreach($headers as $header) {
            $str =  substr($header, 0, 10);
            if( $str == "location: " ) { 
                $target = substr($header, 10);
                $parse = parse_url($target);
                $domain = $parse['host'];
                curl_close($ch);
                return $domain;
            }   
        }   
        curl_close($ch);
        return false;
    }
}
