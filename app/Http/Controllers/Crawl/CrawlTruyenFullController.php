<?php

namespace App\Http\Controllers\Crawl;

use Illuminate\Http\Request;
use App\Http\Controllers\Crawl\CrawlBaseController;
use Harimayco\Menu\Facades\Menu;
use App\Services\CategoryService;
use App\Services\CategoryStoryService;
use App\Services\StoryService;
use App\Services\ChapterService;
use App\Services\ValidationService;
use App\Services\LogsUserService;
use Exception;
use HTMLDomParser;
use Illuminate\Support\Str;
use App\Helpers\UploadImage;
use App\Helpers\Common;
class CrawlTruyenFullController extends CrawlBaseController
{



    public function __construct(Request $request)
    {
        $this->request = $request;
        
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */
    public function crawl()
    {
        $sourceTruyen = Common::sourceTruyen();
        return view('admin.crawlers.index', compact('sourceTruyen'));
    }

    public function crawlChapter()
    {
        $sourceTruyen = Common::sourceTruyen();
        return view('admin.crawlers.chapter', compact('sourceTruyen'));
    }

    public function index()
    {
       
        $url = "https://truyencv.com/pham-nhan-tu-tien-chi-tien-gioi-thien/";
        $html = parent::curl($url);
        $link_id = HTMLDomParser::str_get_html($html)->find('.list-popover li a')[0]->href;
        $list_id = explode('/', $link_id);
        $id = isset($list_id[3]) ? (int) $list_id[3] : 1;
        $slug = explode('/', $url)[3];
        $row = StoryService::findByKey('story_slug', $slug);
        if ($row) {
            $para = 'showChapter=1&media_id=' . $id . '&number=1&page=1&type=' . $slug;


            $html2 = parent::curlPost('https://truyencv.com/index.php', $para);

            $list_chapter = HTMLDomParser::str_get_html($html2)->find('.item a');
            $list_chapter = array_reverse($list_chapter, true);

            // foreach ($list_chapter as $chapter) {
            for ($i = count($list_chapter) - 1195; $i > 0; $i--) {
                //dd($chapter->href);
                //echo $list_chapter[$i]->href.'<br>';

                $data = $this->fillChapter(1, $list_chapter[$i]->href);
                if ($data) {

                    $filter = array('story_id' => $row->id, 'chapter_slug' => $data['chapter_slug']);
                    //dd($filter);
                    $chapter = ChapterService::totalRows($filter);
                    if ($chapter < 1) {
                        $data['story_id'] = $row->id;
                        ChapterService::insert($data);
                    }
                }
            }
        } else {
            echo 'Truyện này chưa có trong dữ liệu <br>';
        }

        die();
    }

    public function listPage()
    {
        for ($i = 205; $i > 1; $i--) {
            echo 'https://truyenfull.vn/danh-sach/truyen-moi/trang-' . $i . '/';
            echo '<br>';
        }

        
    }

    public function ajaxListStory()
    {
        $params = $this->request->only('page');
        $data = array();
        $data['list_story'] = array();
        if (isset($params['page'])) {
            $url = $params['page'];

            $html = parent::curl($url);

            if ($html) {



                $stories_link = HTMLDomParser::str_get_html($html)->find('.list-truyen h3.truyen-title a');
                foreach ($stories_link as $story) {
                    //echo $story->href.'<br>';
                    if($story->href) {
                        array_push($data['list_story'], $story->href);
                    }
                    
                }

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách truyện thành công</strong>';
            } else {
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm HTML</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }
        //echo 'TEST';
        echo json_encode($data);
        die();
    }



    public function ajaxListChapter()
    {
        $params = $this->request->only('url');
        $data = array();
        $data['list_chapter'] = array();
        $story_status = '';
        if (isset($params['url'])) {

            $story = $this->fillStory($params['url']);
            $row = StoryService::findByKey('story_title', $story['story_title']);
            if ($row) {
                $genres = $this->fillGenres($row->id, $params['url']);
                $story_status = 'Truyện ' . $story['story_title'] . ' đã có sẵn ' . $genres;
                $data['message'] = '<strong class="kt-font-warning kt-font-bold">' . $story_status . '</strong>';
            } else {


                if ($story) {
                    $image_url = $story['story_thumbnail'];
                    if ($image_url) {
                        $month = date('m');
                        $year = date('Y');
                        $destinationPath = 'uploads/files/'.$year.'/'.$month.'/';
                        if (!is_dir('./' . $destinationPath)) {
                                mkdir('./' . $destinationPath, 0777, true);
                        }
            
                     $image = $this->grab_image($image_url, $destinationPath . $story['story_slug'] . '.jpg');
                        $story['story_thumbnail'] = $destinationPath . $story['story_slug'] . '.jpg';
                    }
                    $insert_id = StoryService::insert($story);
                    if ($story) {
                        $genres = $this->fillGenres($insert_id, $params['url']);
                        
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thành công ' . $genres;
                        $data['message'] = '<strong class="kt-font-success kt-font-bold">' . $story_status . '</strong>';
                    } else {
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thất bại ';
                        $data['message'] = '<strong class="kt-font-danger kt-font-bold">' . $story_status . '</strong>';
                    }
                } else {
                    $story_status = 'Truyện ' . $story['story_title'] . ' dữ liệu bị lỗi, vui lòng thử lại ';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">' . $story_status . '</strong>';
                }
            }

            // try {

            $data['status']    = 'success';
            //     $this->mergeListChapter($data['list_chapter'], $params['url']);
            //     $count = count($data['list_chapter']);
            //     $data['status'] = 'success';
            //     $data['message'] = '<strong class="kt-font-success kt-font-bold">' . $story_status . ' - Đã lấy ' . $count . ' chapter thành công</strong>';
            // } catch (Exception $e) {
            //     $data['status'] = 'error';
            //     $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, vui lòng thử lại</strong>';
            // }

        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }

        echo json_encode($data);
        die();
    }

    // TruyệnFull
    public function ajaxListChapter2()
    {
        $params = $this->request->only('ids');

        $ids = $params['ids'];
        
        
        $ids = array_unique($ids);
        $data['data'] = array();
        foreach ($ids as $id) {
            $array_chapter = array();
            $row = StoryService::findByKey('id', $id);
            $url = 'https://truyentr.com/truyen/' . $row->story_slug . '/';

            $html = parent::curl($url);
            if(isset($row)) {
                
                if($row->crawl_status == 'done') {
                    $data['status'] = 'error';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">Truyện này đã lấy chương</strong>';
                } else {
                    if ($html) {
                        try {
                            
                            $this->mergeListChapter($array_chapter, $url);

                                
        
                            $data['status'] = 'success';
                            $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách chương thành công</strong>';
                        } catch(Exception $e) {
                            $data['status'] = 'error';
                            $data['message'] = '<strong class="kt-font-success kt-font-bold">Lỗi: '.$e->getMessage().'</strong>';
                        }
                        
        
                       
                    } else {
                        $data['status'] = 'error';
                        $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, không lấy được HTML</strong>';
                    }
                }
            }
            
            array_push($data['data'],array('story_id' => $id, 'story_title' => $row->story_title,'list_chapter' => $array_chapter));
            
        }

        
        echo json_encode($data);
        die();
    }

    public function ajaxUpdateCrawlLogs() {

        try {
            $params = $this->request->only('id');

            $id = $params['id'];
            $params['crawl_status'] = 'done';
            $update_id = StoryService::updateStatusCrawl($id, $params);
            if($update_id) {

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Cập nhật crawl Logs thành công</strong>';
            }
        } catch(Exception $e) {
            
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-success kt-font-bold">Cập nhật crawl Logs thất bại</strong>';
        }
        
        echo json_encode($data);
        die();
    }
    public function ajaxAddChapter()
    {
        $params = $this->request->only('id','url');
        //dd($params);
        $data = $this->fillChapter(1, $params['url']);
        if ($data) {

            
            $filter = array('story_id' => $params['id'], 'chapter_slug' => $data['chapter_slug']);
            //dd($filter);
            $chapter = ChapterService::totalRows($filter);
            if ($chapter < 1) {
                $data['story_id'] = $params['id'];
                $insert_id = ChapterService::insert($data);
                if($insert_id) {
                    StoryService::update($params['id'], null);
                    $data['status'] = 'sucess';
                    $data['message'] = '<strong class="kt-font-success kt-font-bold">'.$data['chapter_title'].' : Đã lấy thành công</strong>';
                } else {
                    $data['status'] = 'sucess';
                    $data['message'] = '<strong class="kt-font-danger kt-font-bold">'.$data['chapter_title'].' : Lấy thất bại</strong>';
                }
            } else {
                $data['status'] = 'sucess';
                $data['message'] = '<strong class="kt-font-warning kt-font-bold">'.$data['chapter_title'].' : Đã có sẵn</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">'.$data['chapter_title'].' : Lỗi khi lấy dữ liệu </strong>';
        }
        echo json_encode($data);
        die();
    }

    public function mergeListChapter(&$list_chapter, $url)
    {

        // $url = str_replace('?trang=', '/?trang=', )
        $html = parent::curl($url);

        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');

            foreach ($chapters as $chapter) {
                array_push($list_chapter, $chapter->href);
            }

            $pagination = HTMLDomParser::str_get_html($html)->find('.pagination-sm li');
            $next = false;
            $url_next = false;
            foreach($pagination as $key => $p) {

                if($next ==  $key) {
                    $url_next = $p->find('a')[0]->href;
                }
                if($p->class == 'active') {
                    $next = $key + 1;
                }

            }
            if ($url_next) {
                self::mergeListChapter($list_chapter, $url_next);
            }
            return true;
        }
        return false;
    }

    public function fillStory($url)
    {
        $html = parent::curl($url);
        $params = array();
        if ($html) {
            $params['story_title'] = isset(HTMLDomParser::str_get_html($html)->find('h3.title')[0]) ? HTMLDomParser::str_get_html($html)->find('h3.title')[0]->plaintext : null;
            if ($params['story_title'] == null) {
                return false;
            } else {
                $params['story_title'] = trim($params['story_title']);
            }
            $params['story_slug'] = Str::slug($params['story_title']);
            $params['story_description'] = HTMLDomParser::str_get_html($html)->find('.desc-text')[0]->innertext;
            $params['story_seo_title'] =  $params['story_title'];
            $params['story_seo_keyword'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content : $params['story_title'];
            $params['story_seo_description'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content : $params['story_title'];
            $params['story_author'] = HTMLDomParser::str_get_html($html)->find('a[itemprop="author"]')[0]->plaintext;
            $params['story_source'] = 'TruyenFull';
            $element_status = HTMLDomParser::str_get_html($html)->find('.info-holder .info span');
            $params['story_status'] = isset($element_status[1]) ? $element_status[1]->plaintext : 'coming';
            // $params['story_status'] = isset(HTMLDomParser::str_get_html($html)->find('.info-holder .info span')[1]) ? HTMLDomParser::str_get_html($html)->find('.info-holder .info span.text-primary')[0]->plaintext : 'coming';

            $params['story_status'] =  trim(str_replace('Hoàn thành', 'full', $params['story_status']));
            $params['story_status'] =  trim(str_replace('Full', 'full', $params['story_status']));
            $params['story_status'] =  trim(str_replace('Đang ra', 'coming', $params['story_status']));
            $params['story_status'] =  trim(str_replace('Tạm dừng', 'pause', $params['story_status']));
            $params['story_status'] =  trim(str_replace('Dừng viết', 'stop', $params['story_status']));
            $params['story_thumbnail'] = HTMLDomParser::str_get_html($html)->find('.info-holder .books .book img')[0]->getAttribute('data-cfsrc');
           


            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            return $params;
        }

        return false;
    }
    public function fillGenres($id, $url)
    {
        $html = parent::curl($url);
        $params = array();
        $genres_text = ' | Thể loại : ';
        if ($html) {
            $genres = HTMLDomParser::str_get_html($html)->find('.info div a[itemprop="genre"]');
            foreach ($genres as $genre) {
                if ($genre->plaintext) {
                    $title = trim($genre->plaintext);
                    $row = CategoryService::findByKey('category_name', trim($title));
                    if ($row) {
                        $params = array('category_id' => $row->id, 'story_id' => $id);
                        $count = CategoryStoryService::totalRows($params);
                        if ($count < 1) {
                            CategoryStoryService::insert($params);
                        }
                        $genres_text .= $title . ', ';
                    }
                }
            }
        }
        return $genres_text;
    }
    public function fillCategories($url)
    {
        $html = parent::curl($url);
        $params = array();
        if ($html) {
            $params['category_name'] = HTMLDomParser::str_get_html($html)->find('span[itemprop="title"]')[1]->plaintext;
            $params['category_description'] = HTMLDomParser::str_get_html($html)->find('.cat-desc .panel-body')[0]->innertext;
            $params['category_seo_title'] = HTMLDomParser::str_get_html($html)->find('title')[0]->plaintext;
            $params['category_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
            $params['category_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;

            return $params;
        }
        return false;
    }

    public function fillChapter($order, $url)
    {
        $data = parent::curl($url);

        $html = HtmlDomParser::str_get_html( $data );

        $params = array();
        if ($html) {

            $excude_array = ['.OUTBRAIN'];
            foreach($excude_array as $ex) {

                foreach($html->find($ex) as $item) {

                    $item->outertext = '';

                }

            }
            //dd($html);
            $params['chapter_title'] = $html->find('.chapter-title')[0]->plaintext;
            $params['chapter_slug'] = explode('/', $url)[5];
            $content_el = $html->find('#chapter-c');
            $content_text = isset($content_el[0]) ? $content_el[0]->innertext : '';

            $content_text =  preg_replace('/<a(.*?)href=\"(.*?)\">(.*?)<\/a>/', "\\3", $content_text);

            $content_text = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $content_text);

            $content_text = str_replace('Truyenfull.net', 'Truyenfull.net', $content_text);

            $content_text = str_replace('truyentr.com', 'truyentr.com', $content_text);

            $params['chapter_content'] = $content_text;

            $params['chapter_seo_title'] =  $params['chapter_title'];
            $params['chapter_seo_keyword'] = $html->find('meta[name="keywords"]')[0]->content;
            $params['chapter_seo_description'] = $html->find('meta[name="description"]')[0]->content;
            $order = str_replace('chuong-', '', $params['chapter_slug']);
            $order = !empty($order) ? (int) $order : 1;
            $params['chapter_order'] = $order;

            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            //dd($params);
            return $params;
        }

        return false;
    }
    public function listChapter($story_id, $url)
    {
        $html = parent::curl($url);
        $params = array();
        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');
            $order = 1;
            foreach ($chapters as $chapter) {
                if ($chapter->href) {
                    $data = $this->fillChapter($order, $chapter->href);
                    if ($data) {
                        $data['story_id'] = $story_id;
                        $params = array('story_id' => $story_id, 'chapter_slug' => $data['chapter_slug']);
                        $count = ChapterService::totalRows($params);
                        if ($count < 1) {
                            ChapterService::insert($data);
                        }
                    }
                }
                $order++;
            }

            $pagination = isset(HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href) ? HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href : false;
            if ($pagination) {
                self::listChapter($story_id, $pagination);
            }
            return true;
        }
        return false;
    }

}
