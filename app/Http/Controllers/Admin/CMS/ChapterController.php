<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\ChapterService;
use App\Services\CategoryService;
use App\Services\CategoryChapterService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
use Exception;

class ChapterController extends Controller
{
    //
    protected $request;
    protected $validator;
    protected $chapterService;

    function __construct(Request $request, ValidationService $validator, ChapterService $chapterService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->chapterService = $chapterService;
    }


    /**
     * METHOD index - View List story
     *
     * @return void
     */

    public function index()
    {

        return view('admin.stories.index');
    }

    /**
     * METHOD index - Ajax Get List story
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        if($params['story_id']){
            $params['query']['story_id'] = $params['story_id'];
        }
        $result = $this->chapterService->getList($params);
       
        return response()->json($result);
    }


    /**
     * METHOD viewInsert - VIEW ADD, EDIT story
     *
     * @return void
     */

    public function add($id = 0)
    {

        $params = array('category_parent' => 0, 'category_type' => 'category_story');
        $categories = CategoryService::getAllByKey(['id', 'category_name'], $params);
        $categories = $this->categoriesCheckbox($categories, [], 0);
        return view('admin.stories.add', ['categoriesCheckbox' =>  $categories]);
    }

    public function addAction()
    {
        $params = $this->request->all();
        $user = auth()->user();
        $params['created_by_user'] = $user->id;
        $params['updated_by_user'] = $user->id;
        $params['chapter_author'] = $user->full_name;

        $validator = $this->validator->make($params, 'add_chapter_fields');

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);
        }

        $story =  $this->ChapterService->findByKey('chapter_title', $params['chapter_title']);
        if($story) {
            Message::alertFlash('Tên truyện này đã tồn tại, vui lòng kiểm tra lại!', 'danger');
            return redirect()->back()->withInput();
        }
        $add = $this->ChapterService->insert($params);

        if ($add) {
            if (isset($params['chapter_categories']) && is_array($params['chapter_categories'])) {

                foreach ($params['chapter_categories'] as $id) {
                    $data = array('category_id' => $id, 'chapter_id' => $add);
                    CategoryChapterService::insert($data);
                }
            }

            Message::alertFlash('Bạn đã thêm bài viết mới thành công', 'success');
            $log['action'] = "Thêm bài viết có ID = " . $add . " thành công";
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);
        } else {

            Message::alertFlash('Đã xảy ra lỗi khi tạo bài viết mới, vui lòng liên hệ quản trị viên!', 'danger');
        }

        return redirect()->back()->withInput();
    }

    public function edit($id)
    {
        
        $params = array('chapter_id' => $id);

        $chapter = $this->chapterService->findByKey('id', $id);
        return view('admin.chapters.edit',  compact('chapter'));
    }

    public function editAction($id)
    {
        try {
            $params = $this->request->all();
            $user = auth()->user();
            $params['created_by_user'] = $user->id;
            $params['updated_by_user'] = $user->id;
            $params['chapter_author'] = $user->full_name;

            $validator = $this->validator->make($params, 'add_chapter_fields');

            if ($validator->fails()) {
                return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);
            }

            $this->chapterService->update($id, $params);

        } catch(Exception $e) {
            Message::alertFlash('Đã xảy ra lỗi '.$e->getMessage().' khi cập nhật bài viết, vui lòng liên hệ quản trị viên!', 'danger');
        }
        

        

        return redirect()->back()->withInput();
    }

    /**
     * METHOD deleteMany - Delete Array Post with IDs
     *
     * @return json
     */


    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->ChapterService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " Bài viết thành công !!!", 'success');

        $log['action'] = "Xóa các bài viết có IDs = " . implode(", ", $params['ids']) . " viết thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " Bài viết thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $delete = $this->ChapterService->delete($id);
        if ($delete) {
            Message::alertFlash('Bạn đã xóa bài viết thành công', 'success');

            $log['action'] = "Xóa bài viết có ID = " . $id . " thành công";
            $log['content'] = '';
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);
        } else {
            Message::alertFlash('Bạn đã xóa bài viết thất bại', 'danger');
        }

        return redirect("story");
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->ChapterService->updateMany($params['ids'], ['chapter_status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " Bài viết thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập các bài viết có IDs = " . implode(", ", $params['ids']) . " viết thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " Bài viết thành công !!!";
        return response()->json($data);
    }


    public function categoriesCheckbox($categories = [], $categories_is_check = array(), $lvl = 0)
    {
        $html = '';
        foreach ($categories as $category) {
            $params = array('category_parent' => $category->id);
            $category->is_check = '';
            if (is_array($categories_is_check)) {
                if (in_array($category->id, $categories_is_check)) {
                    $category->is_check = 'checked';
                }
            }

            $html = $html . '<label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand ml-' . ($lvl * 2) . '">
            <input type="checkbox" name="chapter_categories[]" value="' . $category->id . '" ' . $category->is_check . ' > ' . $category->category_name . '
            <span></span>
        </label>';
            $category->child = CategoryService::getAllByKey(['id', 'category_name'], $params);
            if ($category->child) {
                $html = $html . self::categoriesCheckbox($category->child, $categories_is_check, $lvl + 1);
            }
        }

        return $html;
    }
}
