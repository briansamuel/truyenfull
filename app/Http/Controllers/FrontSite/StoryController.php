<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\StoryService;
use App\Services\ChapterService;
use App\Services\CategoryService;
use App\Services\CategoryStoryService;
use App\Services\ViewService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
use Exception;
use Illuminate\Support\Facades\DB;

class storyController extends Controller
{
    //
    function __construct(Request $request, ValidationService $validator, StoryService $storyService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->storyService = $storyService;
    }


    /**
     * METHOD index - View List story
     *
     * @return void
     */

    public function index($story_slug, $currentPage = 1)
    {
        
        $story = $this->storyService->findbyKey('story_slug', $story_slug);
        
        if(!$story) {
            abort('404');
        }
       
        $pagination = array(
            'perpage' => 5,
            'page' => 1,
            'story_id' => $story->id,
        );
        $params = array(
            
            'story_id' => $story->id,
        );
        $categories = CategoryStoryService::getAllByKey(['category_id'], $params, false);
        $categories_array = [];
        foreach($categories as $item) {
            $category = CategoryService::findByKey('id', $item->category_id);
            $link = '<a itemprop="genre" href="the-loai/'.$category->category_slug.'/" title="'.$category->category_name.'">'.$category->category_name.'</a>';
            array_push($categories_array, $link);
        }
        $story->story_genres = implode(' ,', $categories_array);
        $story->story_status = $story->story_status == 'coming' ? 'Đang ra' : 'Đã Hoàn Thành';
        \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $sort = array('field' => 'id', 'sort' => 'DESC');
        $lastest_chapters = ChapterService::getMany(['chapter_slug', 'chapter_title'], $pagination, $sort, $params);
       
        
        $pagination = DB::table('chapters')->select(['chapter_title', 'chapter_slug', 'chapter_order'])->where('story_id',$story->id)->orderBy('chapter_order')->orderBy('id')->paginate(50);
        
        $top_days = ViewService::getMany(['story_id'], ['perpage' => 10], ['field' => 'view_day', 'sort' => 'DESC'], null)->pluck('story_id')->toArray();
        $top_stories = StoryService::getMany(['id', 'story_title', 'story_slug'], ['perpage' => 10], null, ['ids' => $top_days]);
        $categoriesAll = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);
        $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $top_days], false);
        foreach($top_stories as $key => $s){

            $categoriesS = $categoriesSAll->where('story_id', $s->id);
            $categories_array = [];
            foreach($categoriesS as $item) {
                if($categoriesAll->firstWhere('id',  $item->category_id)) {
                    $c = $categoriesAll->firstWhere('id',  $item->category_id);
                    $link = '<a itemprop="genre" href="the-loai/'.$c->category_slug.'/" title="'.$c->category_name.'">'.$c->category_name.'</a>';
                    array_push($categories_array, $link);
                }
               
            }
            $s->story_genres = implode(' ,', $categories_array);
        }
 
        $top_days_stories = [];
        foreach($top_days as $k => $v) {
            $top_days_stories[] = $top_stories->firstWhere('id', $v);
        }
        
        
        $data['story'] = $story;
        $data['lastest_chapters'] = $lastest_chapters;
        $data['top_days_stories'] = $top_days_stories;
        $data['pagination'] = $pagination;
       
        return view('frontsite.stories.index', $data);
    }
    public function list($slug = '', $currentPage = 1) {
        $stories = [];
        $sort = array('field' => 'updated_at', 'sort' => 'DESC');
        $pagination =  array('perpage' => 20, 'page' => $page = 1);
       
        $pageList = null;
        if($slug == 'truyen-moi') {
            \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });

            $lastest_chapters = ChapterService::getLastestChapterPaginate(['id', 'story_id', 'chapter_slug', 'chapter_title', 'chapter_order', 'updated_at'], $pagination, ['field' => 'id', 'sort' => 'DESC'], null);
            
           
            $lastest_chapters->withPath('/danh-sach/'.$slug.'/');

            $items = $lastest_chapters->items();
            $story_ids = [];
            foreach($items as $i) {
                $story_ids[] = $i->story_id;
            }
            $lastest_stories = StoryService::getMany(['id', 'story_title', 'story_slug','story_author', 'story_thumbnail', 'story_status', 'story_feature', 'updated_at'], $pagination, ['field' => 'updated_at', 'sort' => 'DESC'], ['ids' => $story_ids]);
            
            $stories = [];
            foreach($lastest_chapters as $chapter) {
                $story = $lastest_stories->firstWhere('id', $chapter->story_id);
                $story->lastest_chapter = $chapter;
                array_push($stories, $story);
            }

            $pageList = str_replace('?page=','trang-',$lastest_chapters->toHtml());
            // $stories = StoryService::getManyWithPaginate(['id', 'story_title', 'story_author', 'story_thumbnail', 'story_slug', 'story_status', 'story_feature'],  $pagination, $sort, null);
            // $items = $stories->items();
            // $story_ids = [];
            // foreach($items as $i) {
            //     $story_ids[] = $i->id;
            // }
            // $story_ids_str = implode(', ', $story_ids);
            // $chapters =  ChapterService::getMany(['story_id', 'chapter_slug', 'chapter_title', 'chapter_order'], $pagination, null, ['ids' => array(DB::raw("select max(id) from `shtbl_chapters` where `story_id` in ({$story_ids_str}) GROUP BY story_id"))]);
            // foreach($stories as $key => $story) {
            //     $chapter = $chapters->firstWhere('story_id', $story->id);
            //     $story->lastest_chapter = $chapter;
            // }
        }


        $top_days = ViewService::getMany(['story_id'], ['perpage' => 10], ['field' => 'view_day', 'sort' => 'DESC'], null)->pluck('story_id')->toArray();
        $top_stories = StoryService::getMany(['id', 'story_title', 'story_slug'], ['perpage' => 10], null, ['ids' => $top_days]);
        $categoriesAll = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);
        $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $top_days], false);
        foreach($top_stories as $key => $s){

            $categoriesS = $categoriesSAll->where('story_id', $s->id);
            $categories_array = [];
            foreach($categoriesS as $item) {
                if($categoriesAll->firstWhere('id',  $item->category_id)) {
                    $c = $categoriesAll->firstWhere('id',  $item->category_id);
                    $link = '<a itemprop="genre" href="the-loai/'.$c->category_slug.'/" title="'.$c->category_name.'">'.$c->category_name.'</a>';
                    array_push($categories_array, $link);
                }
               
            }
            $s->story_genres = implode(' ,', $categories_array);
        }
 
        $top_days_stories = [];
        foreach($top_days as $k => $v) {
            $top_days_stories[] = $top_stories->firstWhere('id', $v);
        }

        $data['stories'] = $stories;
        $data['title'] = 'TRUYỆN MỚI CẬP NHẬT';
        $data['top_days_stories'] = $top_days_stories;
        $data['page_list'] = $pageList;
        return view('frontsite.stories.list', $data);
    }
    // Search Method 
    public function search() {
        $params = $this->request->only('tukhoa');
        $keyword= isset($params['tukhoa']) ? $params['tukhoa'] : '';
        $sort = array('field' => 'id', 'sort' => 'DESC');
        $page = isset($params['page']) ? $params['page'] : '';
        $pagination =  array('perpage' => 20, 'page' => $page = 1);
        $stories = StoryService::getManyWithPaginate(['id', 'story_title', 'story_author', 'story_thumbnail', 'story_slug'],  $pagination, $sort, ['story_title' => $keyword]);

        $top_days = ViewService::getMany(['story_id'], ['perpage' => 10], ['field' => 'view_day', 'sort' => 'DESC'], null)->pluck('story_id')->toArray();
        $top_stories = StoryService::getMany(['id', 'story_title', 'story_slug'], ['perpage' => 10], null, ['ids' => $top_days]);
        $categoriesAll = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);
        $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $top_days], false);
        foreach($top_stories as $key => $s){

            $categoriesS = $categoriesSAll->where('story_id', $s->id);
            $categories_array = [];
            foreach($categoriesS as $item) {
                if($categoriesAll->firstWhere('id',  $item->category_id)) {
                    $c = $categoriesAll->firstWhere('id',  $item->category_id);
                    $link = '<a itemprop="genre" href="the-loai/'.$c->category_slug.'/" title="'.$c->category_name.'">'.$c->category_name.'</a>';
                    array_push($categories_array, $link);
                }
               
            }
            $s->story_genres = implode(' ,', $categories_array);
        }
 
        $top_days_stories = [];
        foreach($top_days as $k => $v) {
            $top_days_stories[] = $top_stories->firstWhere('id', $v);
        }

        $data['stories'] = $stories;
        $data['keyword'] = $keyword;
        $data['top_days_stories'] = $top_days_stories;
        $data['page_list'] = str_replace('?page=','?tukhoa='.$keyword.'&page=',$stories->toHtml());
        return view('frontsite.stories.search', $data);
    }
    /**
     * METHOD index - Ajax Get List story
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        $result = $this->storyService->getList($params);
       
        return response()->json($result);
    }

    public function ajaxStory(Request $request)
    {
        $params = $this->request->all();
        $id = isset($params['id']) ? $params['id'] : null;
        $tid = isset($params['tid']) ? $params['tid'] : null;
        $tascii = isset($params['tascii']) ? $params['tascii'] : null;
        $page = isset($params['page']) ? $params['page'] : 1;
        $type = isset($params['type']) ? $params['type'] : '';
        $data_type = isset($params['data_type']) ? $params['data_type'] : 'view_month';
        $str = isset($params['str']) ? $params['str'] : '';
        switch($type)
        {
            case 'new_select': 
                return $this->loadLastestStories($id);
                break;
            case 'list_chapter': 
                return $this->loadListChapter($tid, $tascii);
                break;
            case 'update_views':  
                return $this->updateView($tid);
                break;
            case 'chapter_view_history':  
                return $this->updateView($tid);
                break;
            case 'top_switch':  
                return $this->loadTopStories($data_type);
                break;
            case 'quick_search':  
                return $this->loadQuickSearch($str);
                break;
            case 'hot_select':  
                return $this->loadHotSelect($id);
                break;
            case 'read_history':  
                return response()->json($this->loadHistory());
                break;
            default: 
                return false;
                break;
        }
        // $pagination  = DB::table('chapters')->select(['chapter_title', 'chapter_slug'])->where('story_id',$id)->orderBy('id','ASC')->paginate(50);
        // // $compact['paginator']
        // //$data['page_list'] = view('frontsite.elements.pagination')->with('paginator',$pagination)->render();
        // $pagination->withPath('/'.$slug.'/');
        // $element['story'] = (object) array(
        //     'id' => $params['tid'],
        //     'story_slug' => $params['tascii'],
        // );
        // $element['pagination'] = $pagination;
        
        // $data['chap_list'] = view('frontsite.stories.elements.list-chapter', $element)->render();
        // $data['page_list'] = str_replace('/?page=','/trang-',$pagination->toHtml());
        // return response($data)
        //     ->header('Content-Type', 'JSON')
        //     ->header('X-Header-One', 'Header Value')
        //     ->header('X-Header-Two', 'Header Value');
        die();    
    }
    
    // read History 
    public function loadHistory() {
        $history = null !== $this->request->cookie('list_history') ? unserialize($this->request->cookie('list_history')) : [];
        $stories = is_array($history) ? $history : [];
       
        $main = view('frontsite.home.elements.list-history-main', compact('stories'))->render();
        $side = view('frontsite.home.elements.list-history', compact('stories'))->render();

        return array('main' => $main, 'side' => $side); 
    }

    public function updateView($story_id) {
        if($story_id) {
            $view_row =  DB::table('views')->where('story_id', $story_id)->first();
            if($view_row) {
                DB::table('views')->where('story_id', $story_id)->update(['view_day' => DB::raw('view_day + 1'), 'view_month' => DB::raw('view_month + 1'), 'view_year' => DB::raw('view_year + 1'), 'view_week' => DB::raw('view_week + 1'), 'updated_at' => date("Y-m-d H:i:s")]);
            } else {
                $params = array(
                    'view_day' => 1,
                    'view_month' => 1,
                    'view_year' => 1,
                    'view_week' => 1,
                    'story_id' => $story_id,
                    'created_at' =>  date("Y-m-d H:i:s"),
                    'updated_at' =>  date("Y-m-d H:i:s"),
                );
                try {
                    DB::table('views')->insert($params);
                } catch(Exception $e) {
                    dd($e);
                }
                
            }
           
        }
       
    }

    public function loadHotSelect($id) {
       
       

        $filter = [];
        if($id != 'all') {
            $story_ids = CategoryStoryService::getMany(['category_id', 'story_id'], ['perpage' => 13], ['field' => 'story_id', 'sort' => 'DESC'], ['category_id' => $id])->pluck('story_id')->toArray();
            $filter = array('ids' => $story_ids);
        }
        
        $pagination = array('perpage' => 13, 'page' => 1);
       
        $sort = array('field' => 'id', 'sort' => 'DESC');

        $hot_stories = StoryService::getMany(['story_title', 'story_thumbnail', 'story_slug', 'story_status'], $pagination, $sort, $filter);
        
        return view('frontsite.home.elements.ajax_hot-stories', compact('hot_stories'));
    }
    public function loadQuickSearch($q) {
        $stories = StoryService::getMany(['id', 'story_title', 'story_slug'], ['perpage' => 5], null, ['story_title' => $q]);
        $html = '';
        foreach($stories as $story) {
            $html .= '<a href="'.url($story->story_slug).'" class="list-group-item" title="'.$story->story_title.'">'.$story->story_title.'</a>';
        }
        if(count($stories) > 4) {
            $html .= '<a href='.url('tim-kiem/?tukhoa=ly hôn').'" class="list-group-item" title="Xem thêm kết quả khác"><i>Xem thêm kết quả khác <span class="glyphicon glyphicon-search"></span></i></a>';
        }
        return $html;
    }
    public function loadTopStories($type) {

        $view = 'view_day';
        if($type == 'all') {
            $view == 'view_year';
        } else if($type == 'month') {
            $view == 'view_month';
        }
        $top_days = ViewService::getMany(['story_id'], ['perpage' => 10], ['field' => 'view_day', 'sort' => 'DESC'], null)->pluck('story_id')->toArray();
        $top_stories = StoryService::getMany(['id', 'story_title', 'story_slug'], ['perpage' => 10], null, ['ids' => $top_days]);
        $categoriesAll = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);
        $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $top_days], false);
        foreach($top_stories as $key => $s){

            $categoriesS = $categoriesSAll->where('story_id', $s->id);
            $categories_array = [];
            foreach($categoriesS as $item) {
                if($categoriesAll->firstWhere('id',  $item->category_id)) {
                    $c = $categoriesAll->firstWhere('id',  $item->category_id);
                    $link = '<a itemprop="genre" href="the-loai/'.$c->category_slug.'/" title="'.$c->category_name.'">'.$c->category_name.'</a>';
                    array_push($categories_array, $link);
                }
               
            }
            $s->story_genres = implode(' ,', $categories_array);
        }
 
        $top_days_stories = [];
        foreach($top_days as $k => $v) {
            $top_days_stories[] = $top_stories->firstWhere('id', $v);
        }
        $html = '';
        foreach($top_days_stories as $key => $s) {
            $html .= '<div class="row top-item" style="display: table-row;"><div class="col-xs-12"><div class="top-num top-'.($key+1).'">'.($key+1).'</div><div class="s-title"><h3><a href="'.url($s->story_slug).'" title="'.$s->story_title.'">'.$s->story_title.'</a></h3></div><div>'.$s->story_genres.'</div></div></div>';
        }
        return $html;
    }

    public function loadListChapter($id, $slug) {


        $pagination = DB::table('chapters')->select(['chapter_title', 'chapter_slug', 'chapter_order'])->where('story_id',$id)->orderBy('chapter_order')->orderBy('id')->paginate(50);
        // $compact['paginator']
        //$data['page_list'] = view('frontsite.elements.pagination')->with('paginator',$pagination)->render();
        $pagination->withPath('/'.$slug.'/');
        $element['story'] = (object) array(
            'id' => $id,
            'story_slug' => $slug,
        );
        $element['pagination'] = $pagination;
        
        $data['chap_list'] = view('frontsite.stories.elements.list-chapter', $element)->render();
        $data['page_list'] = str_replace('/?page=','/trang-',$pagination->toHtml());
        return response($data)
            ->header('Content-Type', 'JSON')
            ->header('X-Header-One', 'Header Value')
            ->header('X-Header-Two', 'Header Value');
    }

    public function loadLastestStories($id)
    {   
        $params = array(
            'perpage' => 21,
            'page' => 1,
        );
        $sort = array('field' => 'id', 'sort' => 'DESC');
        $where_clause = '';
        if($id != 'all') {
            $lastest_chapters =  ChapterService::getMany(['story_id', 'chapter_slug', 'chapter_title', 'chapter_order', 'updated_at'], $params, $sort, ['ids' => array(DB::raw("select max(`id`) from shtbl_chapters GROUP BY story_id")), 'story_ids' => array(DB::raw("select story_id FROM shtbl_category_story_relations WHERE category_id = {$id}"))]);
        } else {
            $lastest_chapters =  ChapterService::getMany(['id','story_id', 'chapter_slug', 'chapter_title', 'chapter_order', 'created_at', 'updated_at'], $params, $sort, ['ids' => array(DB::raw("select max(`id`) from shtbl_chapters GROUP BY story_id"))]);
        }
        
       
        
        if(!empty($lastest_chapters))
        {
            $story_ids = $lastest_chapters->pluck('story_id')->toArray();
            $sort_story = array('field' => 'updated_at', 'sort' => 'DESC');
            $lastest_stories = StoryService::getMany(['id', 'story_title', 'story_slug','story_feature', 'story_status', 'updated_at'], $params, null, ['ids' => $story_ids]);
            $categoriesAll = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);
            $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $story_ids], false);
            foreach($lastest_chapters as $chapter) {

            
                $story = $lastest_stories->firstWhere('id', $chapter->story_id);
     
                // $story->chapter_title =  isset($chapter_row->chapter_title) ? $chapter_row->chapter_title : 'Đang cập nhật';
                // $story->chapter_slug = isset($chapter_row->chapter_slug) ? $chapter_row->chapter_slug : '';
                $chapter_order =   isset($chapter->chapter_slug) ? str_replace('chuong-', 'Chương ', $chapter->chapter_slug) : '';
                $chapter_order =  str_replace('quyen-', 'Quyển ', $chapter_order);
                $chapter->chapter_order =  str_replace('-', ' ', $chapter_order);
             
                $chapter->story_title = isset($story->story_title) ? $story->story_title : '';
                $chapter->story_slug = isset($story->story_slug) ? $story->story_slug : '';
                $chapter->story_status = isset($story->story_status) ? $story->story_status : '';
                $chapter->story_feature = isset($story->story_feature) ? $story->story_feature : '';
    
                $categoriesStory = $categoriesSAll->where('story_id', $story->id);
                $categories_array = [];
                foreach($categoriesStory as $cs) {
                    if($categoriesAll->firstWhere('id', $cs->category_id)) {
                        $category = $categoriesAll->firstWhere('id', $cs->category_id);
                        $link = '<a itemprop="genre" href="the-loai/'.$category->category_slug.'/" title="'.$category->category_name.'">'.$category->category_name.'</a>';
                        array_push($categories_array, $link);
                    }
                   
                }
                $chapter->story_genres = implode(' ,', $categories_array);
            }
        }
        

        return view('frontsite.home.elements.lastest-stories', ['lastest_stories' => $lastest_chapters]);
        
    }

    
}
