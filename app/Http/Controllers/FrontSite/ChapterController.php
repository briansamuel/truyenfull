<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\StoryService;
use App\Services\ChapterService;
use App\Services\CategoryService;
use App\Services\CategoryChapterService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
use Exception;
use Illuminate\Support\Facades\Cookie;

class ChapterController extends Controller
{
    //
    protected $request;
    protected $validator;
    protected $chapterService;

    function __construct(Request $request, ValidationService $validator, ChapterService $chapterService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->chapterService = $chapterService;
    }


    /**
     * METHOD index - View List story
     *
     * @return void
     */

    public function index($story_slug, $chapter_slug)
    {   

        $history = [];
        try {
            $history = $this->request->cookie('list_history');
            $history = null !== $history ? unserialize($history) : null ;

      
        }
        catch(Exception $e) {

        }
        $history = is_array($history) ? $history : [];
        $story = StoryService::findbyKey('story_slug', $story_slug, ['id','story_title', 'story_slug', 'story_thumbnail']);
        $params = array('story_id' => $story->id, 'chapter_slug' => $chapter_slug);
        $chapter = $this->chapterService->findByParams(['id','chapter_title', 'chapter_slug', 'chapter_content', 'chapter_order'], $params);
        if(!$chapter) {
            abort('404');
        }
        $filter = [];
        if($chapter->chapter_order == 0) {
            $params = array('story_id' => $story->id, 'id' => $chapter->id);
        } else {
            $params = array('story_id' => $story->id, 'chapter_order' => $chapter->chapter_order);
        }
       
        $data['nextChapter'] = $this->chapterService->nextChapter(['id','chapter_title', 'chapter_slug', 'chapter_order'], $params);
        $data['previousChapter'] = $this->chapterService->previousChapter(['id','chapter_title', 'chapter_slug', 'chapter_order'], $params);
        $data['chapter'] = $chapter;
        $data['story'] = $story;
        $args_cookie = array(
            'story_slug' => $story->story_slug,
            'story_title' => $story->story_title,
            'chapter_id' => $chapter->id,
            'chapter_slug' => $chapter->chapter_slug,
            'chapter_order' => $chapter->chapter_order,
        );
        $exist = false;
        if(!empty($history)) {
            
           
            foreach($history as $item) {
                if(isset($item['chapter_id']) && $item['chapter_id'] == $args_cookie['chapter_id']){
                    $exist = true;
                }
               
            }
        }
        if(empty($history)) {
            
            array_unshift($history, $args_cookie);
        } elseif(count($history) < 5 ) {
            $hasStory = false;
            foreach($history as $k => &$i) {
                if($i['story_slug'] == $args_cookie['story_slug']) {
                   
                    if($args_cookie['chapter_id'] > $i['chapter_id']) {
                        $i = $args_cookie;
                    }
                    $hasStory = true;
                }
            }
            if(!$hasStory) {
               
                array_unshift($history, $args_cookie);
            }
        } else {
            $hasStory = false;
            foreach($history as $k => &$i) {
                if($i['story_slug'] == $args_cookie['story_slug']) {
                   
                    if($args_cookie['chapter_id'] > $i['chapter_id']) {
                        $i = $args_cookie;
                    }
                    $hasStory = true;
                }
            }
            if(!$hasStory) {
                array_pop($history);
                array_unshift($history, $args_cookie);
            }
           
        }
        if(!$exist) {
            Cookie::queue('list_history', serialize($history), 3600*12);
        }
        

        // $data['nextChapter'] = empty($data['nextChapter']) ? null : $data['nextChapter'];
        // $data['previousChapter'] = empty($data['nextCpreviousChapterhapter']) ? null : $data['previousChapter'];
        // dd($data);
        return view('frontsite.chapters.index', $data);
    }

    /**
     * METHOD index - Ajax Get List story
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        if($params['story_id']){
            $params['query']['story_id'] = $params['story_id'];
        }
        $result = $this->chapterService->getList($params);
       
        return response()->json($result);
    }


    
}
