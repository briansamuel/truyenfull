<?php

namespace App\Http\Controllers\FrontSite;

use App\Services\ValidationService;
use App\Services\StoryService;
use App\Services\ChapterService;
use App\Services\CategoryService;
use App\Services\CategoryStoryService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    
    protected $request;
    protected $validator;

    public function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
        
    }

    /** 
    * ======================
    * Method:: INDEX
    * ======================
    */

    public function index()
    {
        $params = array(
            'perpage' => 21,
            'page' => 1,
        );
        $group = array('field' => 'story_id');
        $sort = array('field' => 'id', 'sort' => 'DESC');
        $lastest_chapter = ChapterService::getLastestChapter(['id', 'story_id', 'chapter_slug', 'chapter_title', 'chapter_order', 'updated_at'], $params, ['field' => 'id', 'sort' => 'DESC'], null);
       
        $story_ids = $lastest_chapter->pluck('story_id')->toArray();
        $lastest_stories = StoryService::getMany(['id', 'story_title', 'story_slug', 'story_status', 'story_feature', 'updated_at'], $params, ['field' => 'updated_at', 'sort' => 'DESC'], ['ids' => $story_ids]);
       
        $categories = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);

        $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $story_ids], false);
       
        foreach($lastest_chapter as $chapter) {

            
            $story = $lastest_stories->firstWhere('id', $chapter->story_id);
 
            // $story->chapter_title =  isset($chapter_row->chapter_title) ? $chapter_row->chapter_title : 'Đang cập nhật';
            // $story->chapter_slug = isset($chapter_row->chapter_slug) ? $chapter_row->chapter_slug : '';
            $chapter_order =   isset($chapter->chapter_slug) ? str_replace('chuong-', 'Chương ', $chapter->chapter_slug) : '';
            $chapter_order =  str_replace('quyen-', 'Quyển ', $chapter_order);
            $chapter->chapter_order =  str_replace('-', ' ', $chapter_order);
         
            $chapter->story_title = isset($story->story_title) ? $story->story_title : '';
            $chapter->story_slug = isset($story->story_slug) ? $story->story_slug : '';
            $chapter->story_status = isset($story->story_status) ? $story->story_status : '';
            $chapter->story_feature = isset($story->story_feature) ? $story->story_feature : '';

            $categoriesStory = $categoriesSAll->where('story_id', $story->id);
            $categories_array = [];
            foreach($categoriesStory as $cs) {
                if($categories->firstWhere('id', $cs->category_id)) {
                    $category = $categories->firstWhere('id', $cs->category_id);
                    $link = '<a itemprop="genre" href="the-loai/'.$category->category_slug.'/" title="'.$category->category_name.'">'.$category->category_name.'</a>';
                    array_push($categories_array, $link);
                }
               
            }
            $chapter->story_genres = implode(' ,', $categories_array);
        }
        
        $pagination = array('perpage' => 13, 'page' => 1);
        $filter = array();
        $hot_stories = StoryService::getMany(['story_title', 'story_thumbnail', 'story_slug', 'story_status'], $pagination, $sort, $filter);

        $pagination = array('perpage' => 21, 'page' => 1);
        $filter = array('status' => 'full');

        
        $full_stories = StoryService::getMany(['id','story_title', 'story_thumbnail', 'story_slug'], ['perpage' => 12], $sort, $filter);
        foreach($full_stories as $story) {
           
            $story->total_chapter = ChapterService::totalRows(array('story_id' => $story->id));
            
        }

       
        $data['hot_stories'] = $hot_stories;
        $data['lastest_stories'] = $lastest_chapter;
        $data['full_stories'] = $full_stories;
      
        return view('frontsite.home.index', $data);
    }

    
}
