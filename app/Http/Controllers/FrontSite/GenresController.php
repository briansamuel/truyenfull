<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\StoryService;
use App\Services\ChapterService;
use App\Services\CategoryService;
use App\Services\CategoryChapterService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
use App\Services\CategoryStoryService;
use Exception;
use App\Services\ViewService;
use Illuminate\Support\Facades\DB;
class GenresController extends Controller
{
    //
    protected $request;
    protected $validator;


    function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;

    }


    /**
     * METHOD index - View List story
     *
     * @return void
     */

    public function index($genres_slug, $currentPage = 1)
    {
        $category = CategoryService::findByKey('category_slug', $genres_slug);
        $params = array('category_id' => $category->id);
        \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        $pagination = CategoryStoryService::getAllByKey(['category_id', 'story_id'], $params, true)->orderBy('story_id', 'DESC')->paginate(20);
        $pagination->withPath('/the-loai/'.$genres_slug.'/');
        
        $stories = [];
        $items = $pagination->items();
        $story_ids = [];
        foreach($items as $i) {
            $story_ids[] = $i->story_id;
        }
        $story_ids_str = implode(', ', $story_ids);
        $stories = StoryService::getMany(['id', 'story_title', 'story_author', 'story_slug', 'story_thumbnail', 'story_status', 'story_feature'], ['perpage' => 20], null, ['ids' => $story_ids]);
        $chapters =  ChapterService::getMany(['story_id', 'chapter_slug', 'chapter_title', 'chapter_order'], $params, null, ['ids' => array(DB::raw("select max(id) from `shtbl_chapters` where `story_id` in ({$story_ids_str}) GROUP BY story_id"))]);
        $sort = array('field' => 'id', 'sort' => 'DESC');
        $params = array( 'perpage' => 1, 'page' => 1 );
        foreach($stories as $story) {
            if($story->story_thumbnail != '') {
                $story->story_thumbnail = str_replace('uploads/', 'cover/', $story->story_thumbnail);
            } else {
                $story->story_thumbnail = 'cover/files/default-thumbnail.jpg';
            }
            $filter = array('story_id' => $story->id);
            $lastest_chapters = $chapters->firstWhere('story_id', $story->id);
            $lastest_chapter = isset($lastest_chapters) ? $lastest_chapters : NULL;
            $story->lastest_chapter = $lastest_chapter;
        }
        
        $top_days = ViewService::getMany(['story_id'], ['perpage' => 10], ['field' => 'view_day', 'sort' => 'DESC'], null)->pluck('story_id')->toArray();
        $top_stories = StoryService::getMany(['id', 'story_title', 'story_slug'], ['perpage' => 10], null, ['ids' => $top_days]);
        $categoriesAll = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);
        $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $top_days], false);
        foreach($top_stories as $key => $s){

            $categoriesS = $categoriesSAll->where('story_id', $s->id);
            $categories_array = [];
            foreach($categoriesS as $item) {
                if($categoriesAll->firstWhere('id',  $item->category_id)) {
                    $c = $categoriesAll->firstWhere('id',  $item->category_id);
                    $link = '<a itemprop="genre" href="the-loai/'.$c->category_slug.'/" title="'.$c->category_name.'">'.$c->category_name.'</a>';
                    array_push($categories_array, $link);
                }
               
            }
            $s->story_genres = implode(' ,', $categories_array);
        }
 
        $top_days_stories = [];
        foreach($top_days as $k => $v) {
            $top_days_stories[] = $top_stories->firstWhere('id', $v);
        }

        $data['category'] = $category;
        $data['stories'] = $stories;
        $data['top_days_stories'] = $top_days_stories;
        $data['pagination'] = $pagination;
        $data['page_list'] = str_replace('/?page=','/trang-',$pagination->toHtml());
       
        return view('frontsite.genres.index', $data);
    }

    public function indexStatus($genres_slug, $status = 'full', $currentPage = 1)
    {
        if($status == 'hoan') {
            $status == 'full';
        }
        $category = CategoryService::findByKey('category_slug', $genres_slug);
        $params = array('category_id' => $category->id);
        \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        $pagination = CategoryStoryService::getAllByKey(['category_id', 'story_id'], $params, true)->orderBy('story_id', 'DESC')->paginate(20);
        $pagination->withPath('/the-loai/'.$genres_slug.'/');

        $items = $pagination->items();
        $story_ids = [];
        foreach($items as $i) {
            $story_ids[] = $i->story_id;
        }
        $story_ids_str = implode(', ', $story_ids);
        $stories = StoryService::getMany(['id', 'story_title', 'story_author', 'story_slug', 'story_thumbnail', 'story_status', 'story_feature'], ['perpage' => 20], null, ['ids' => $story_ids]);
        $chapters =  ChapterService::getMany(['story_id', 'chapter_slug', 'chapter_title', 'chapter_order'], $params, ['story_status' => $status], ['ids' => array(DB::raw("select max(id) from `shtbl_chapters` where `story_id` in ({$story_ids_str}) GROUP BY story_id"))]);
        $sort = array('field' => 'id', 'sort' => 'DESC');
        $params = array( 'perpage' => 1, 'page' => 1 );
        foreach($stories as $story) {
            if($story->story_thumbnail != '') {
                $story->story_thumbnail = str_replace('uploads/', 'cover/', $story->story_thumbnail);
            } else {
                $story->story_thumbnail = 'cover/files/default-thumbnail.jpg';
            }
            $filter = array('story_id' => $story->id);
            $lastest_chapters = $chapters->firstWhere('story_id', $story->id);
            $lastest_chapter = isset($lastest_chapters) ? $lastest_chapters : NULL;
            $story->lastest_chapter = $lastest_chapter;
        }

        
        $top_days = ViewService::getMany(['story_id'], ['perpage' => 10], ['field' => 'view_day', 'sort' => 'DESC'], null)->pluck('story_id')->toArray();
        $top_stories = StoryService::getMany(['id', 'story_title', 'story_slug'], ['perpage' => 10], null, ['ids' => $top_days]);
        $categoriesAll = CategoryService::getAllByKey(['id', 'category_name', 'category_slug'], null);
        $categoriesSAll = CategoryStoryService::getAllByKey(['story_id','category_id'], ['story_ids' => $top_days], false);
        foreach($top_stories as $key => $s){

            $categoriesS = $categoriesSAll->where('story_id', $s->id);
            $categories_array = [];
            foreach($categoriesS as $item) {
                if($categoriesAll->firstWhere('id',  $item->category_id)) {
                    $c = $categoriesAll->firstWhere('id',  $item->category_id);
                    $link = '<a itemprop="genre" href="the-loai/'.$c->category_slug.'/" title="'.$c->category_name.'">'.$c->category_name.'</a>';
                    array_push($categories_array, $link);
                }
               
            }
            $s->story_genres = implode(' ,', $categories_array);
        }
 
        $top_days_stories = [];
        foreach($top_days as $k => $v) {
            $top_days_stories[] = $top_stories->firstWhere('id', $v);
        }

        $data['category'] = $category;
        $data['stories'] = $stories;
        $data['pagination'] = $pagination;
        $data['top_days_stories'] = $top_days_stories;
        $data['page_list'] = str_replace('/?page=','/trang-',$pagination->toHtml());
       
        return view('frontsite.genres.index', $data);
    }
    /**
     * METHOD index - Ajax Get List story
     *
     * @return void
     */

    public function cover($thumbnail_name, $extension) {
        
        $filename = 'uploads/files/'.$thumbnail_name.'.'.$extension;
        try {
            $image = imagecreatefromjpeg($filename);
        

            $thumb_width = 180;
            $thumb_height = 80;

            $width = imagesx($image);
            $height = imagesy($image);

            $original_aspect = $width / $height;
            $thumb_aspect = $thumb_width / $thumb_height;

            if ( $original_aspect >= $thumb_aspect )
            {
            // If image is wider than thumbnail (in aspect ratio sense)
            $new_height = $thumb_height;
            $new_width = $width / ($height / $thumb_height);
            }
            else
            {
            // If the thumbnail is wider than the image
            $new_width = $thumb_width;
            $new_height = $height / ($width / $thumb_width);
            }

            $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

            // Resize and crop
            imagecopyresampled($thumb,
                            $image,
                            0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                            0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                            0, 0,
                            $new_width, $new_height,
                            $width, $height);
            header("Content-Type: image/png");                      
            imagejpeg($thumb);
            imagedestroy($thumb);

            exit();
        } catch(Exception $e) {
            
        }
        
       
    }


    public function coverYearMonth($year, $month, $thumbnail_name, $extension) {
        
        $filename = 'uploads/files/'.$year.'/'.$month.'/'.$thumbnail_name.'.'.$extension;
        try {
            $image = imagecreatefromjpeg($filename);
        } catch(Exception $e) {
            $image = imagecreatefrompng($filename);
        }
        
        
        $new_des = 'cover/'.$year.'/'.$month.'/'.$thumbnail_name.'.'.$extension;
        $thumb_width = 180;
        $thumb_height = 80;

        $width = imagesx($image);
        $height = imagesy($image);

        $original_aspect = $width / $height;
        $thumb_aspect = $thumb_width / $thumb_height;

        if ( $original_aspect >= $thumb_aspect )
        {
        // If image is wider than thumbnail (in aspect ratio sense)
        $new_height = $thumb_height;
        $new_width = $width / ($height / $thumb_height);
        }
        else
        {
        // If the thumbnail is wider than the image
        $new_width = $thumb_width;
        $new_height = $height / ($width / $thumb_width);
        }

        $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
        
        // Resize and crop
        imagecopyresampled($thumb,
                        $image,
                        0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                        0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                        0, 0,
                        $new_width, $new_height,
                        $width, $height);
        header("Content-Type: image/jpeg");                      
        imagejpeg($thumb);
        
        imagedestroy($thumb);
        exit();
    }
}
