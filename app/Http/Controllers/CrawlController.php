<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;
use App\Services\CategoryService;
use App\Services\CategoryStoryService;
use App\Services\StoryService;
use App\Services\ChapterService;
use App\Services\ValidationService;
use App\Services\LogsUserService;
use Exception;
use HTMLDomParser;
use Illuminate\Support\Str;

class CrawlController extends Controller
{



    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */
    public function crawl()
    {
        return view('admin.crawlers.index');
    }

    public function index()
    {
        $url = "https://truyenfull.net/the-loai/kiem-hiep/";
        $html = $this->curl($url);
        $stories_link = HTMLDomParser::str_get_html($html)->find('.truyen-title a');
        foreach ($stories_link as $story) {
            //echo $story->href.'<br>';

            $row = StoryService::findByKey('story_title', $story->plaintext);
            if ($row) {
                echo 'Truyện ' . $story->plaintext . ' đã có sẵn <br>';
                $this->listChapter($row->id, $story->href);
            } else {
                $data = $this->fillStory($story->href);

                if ($data) {
                    $insert_id = StoryService::insert($data);
                    if ($story) {
                        $this->fillGenres($insert_id, $story->href);
                        echo 'Truyện ' . $story->plaintext . ' thêm thành công <br>';
                    } else {
                        echo 'Truyện ' . $story->plaintext . ' thêm thất bại <br>';
                    }
                }
            }
        }
        // $url ="https://truyenfull.net/";
        // $html = $this->curl($url);
        // $categories = HTMLDomParser::str_get_html($html)->find('.col-truyen-side .list-cat a');

        // foreach($categories as $category) {

        //     //echo $story->href.'<br>';

        //     $row = CategoryService::findByKey('category_name', $category->plaintext);
        //     if($row) {
        //         echo 'Thể loại '.$category->plaintext.' đã có sẵn <br>';
        //     } else {
        //         $data = $this->fillCategories($category->href);
        //         if($data) {
        //             $insert = CategoryService::insert($data);
        //             if($insert) {
        //                 echo 'Thể loại '.$category->plaintext.' thêm thành công <br>';
        //             } else {
        //                 echo 'Thể loại '.$category->plaintext.' thêm thất bại <br>';
        //             }
        //         }


        //     }
        // }
    }
    public function ajaxListStory()
    {
        $params = $this->request->only('page');
        $data = array();
        $data['list_story'] = array();
        if (isset($params['page'])) {
            $url = $params['page'];

            $html = $this->curl($url);

            if ($html) {



                $stories_link = HTMLDomParser::str_get_html($html)->find('.truyen-title a');
                foreach ($stories_link as $story) {
                    //echo $story->href.'<br>';

                    array_push($data['list_story'], $story->href);
                }

                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">Lấy danh sách truyện thành công</strong>';
            } else {
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm HTML</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }
        //echo 'TEST';
        echo json_encode($data);
        die();
    }



    public function ajaxListChapter()
    {
        $params = $this->request->only('url');
        $data = array();
        $data['list_chapter'] = array();
        $story_status = '';
        if (isset($params['url'])) {

            $story = $this->fillStory($params['url']);
            $row = StoryService::findByKey('story_title', $story['story_title']);
            if ($row) {
                $story_status = 'Truyện ' . $story['story_title'] . ' đã có sẵn ';
            } else {


                if ($story) {
                    $insert_id = StoryService::insert($story);
                    if ($story) {
                        $this->fillGenres($insert_id, $params['url']);
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thành công ';
                    } else {
                        $story_status = 'Truyện ' . $story['story_title'] . ' thêm thất bại ';
                    }
                } else {
                    $story_status = 'Truyện ' . $story['story_title'] . ' dữ liệu bị lỗi, vui lòng thử lại ';
                }
            }

            try {


                $this->mergeListChapter($data['list_chapter'], $params['url']);
                $count = count($data['list_chapter']);
                $data['status'] = 'success';
                $data['message'] = '<strong class="kt-font-success kt-font-bold">' . $story_status . ' - Đã lấy ' . $count . ' chapter thành công</strong>';
            } catch (Exception $e) {
                $data['status'] = 'error';
                $data['message'] = '<strong class="kt-font-danger kt-font-bold">Lỗi khi lấy dữ liệu, vui lòng thử lại</strong>';
            }
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }

        echo json_encode($data);
        die();
    }

    public function mergeListChapter(&$list_chapter, $url)
    {

        $html = $this->curl($url);

        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');

            foreach ($chapters as $chapter) {
                array_push($list_chapter, $chapter->href);
            }

            $pagination = isset(HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href) ? HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href : false;
            if ($pagination) {
                self::mergeListChapter($list_chapter, $pagination);
            }
            return true;
        }
        return false;
    }

    public function fillStory($url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $params['story_title'] = HTMLDomParser::str_get_html($html)->find('h3.title')[0]->plaintext;
            $params['story_slug'] = Str::slug($params['story_title']);
            $params['story_description'] = HTMLDomParser::str_get_html($html)->find('.desc-text')[0]->innertext;
            $params['story_seo_title'] =  $params['story_title'];
            $params['story_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
            $params['story_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
            $params['story_author'] = HTMLDomParser::str_get_html($html)->find('a[itemprop="author"]')[0]->plaintext;
            $params['story_source'] = isset(HTMLDomParser::str_get_html($html)->find('.info span.source')[0]) ? HTMLDomParser::str_get_html($html)->find('.info span.source')[0]->plaintext : 'TruyenFull';
            $params['story_status'] = isset(HTMLDomParser::str_get_html($html)->find('.info span.text-primary')[0]) ? HTMLDomParser::str_get_html($html)->find('.info span.text-primary')[0]->plaintext : 'coming';

            $params['story_status'] =  str_replace('Full', 'full', $params['story_status']);
            $params['story_status'] =  str_replace('Đang ra', 'coming', $params['story_status']);
            $image_url = HTMLDomParser::str_get_html($html)->find('.books img')[0]->src;
            $image = $this->grab_image($image_url, 'uploads/files/' . $params['story_slug'] . '.jpg');
            $params['story_thumbnail'] = 'http://' . config('domain.web.domain') . '/uploads/files/' . $params['story_slug'] . '.jpg';

            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            return $params;
        }

        return false;
    }
    public function fillGenres($id, $url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $genres = HTMLDomParser::str_get_html($html)->find('a[itemprop="genre"]');
            foreach ($genres as $genre) {
                if ($genre->plaintext) {
                    $title = $genre->plaintext;
                    $row = CategoryService::findByKey('category_name', trim($title));
                    if ($row) {
                        $params = array('category_id' => $row->id, 'story_id' => $id);
                        $count = CategoryStoryService::totalRows($params);
                        if ($count < 1) {
                            CategoryStoryService::insert($params);
                        }
                    }
                }
            }
        }
        return true;
    }
    public function fillCategories($url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $params['category_name'] = HTMLDomParser::str_get_html($html)->find('span[itemprop="title"]')[1]->plaintext;
            $params['category_description'] = HTMLDomParser::str_get_html($html)->find('.cat-desc .panel-body')[0]->innertext;
            $params['category_seo_title'] = HTMLDomParser::str_get_html($html)->find('title')[0]->plaintext;
            $params['category_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
            $params['category_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;

            return $params;
        }
        return false;
    }

    public function fillChapter($order, $url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $params['chapter_title'] = HTMLDomParser::str_get_html($html)->find('.chapter-title')[0]->plaintext;
            $params['chapter_slug'] = Str::slug(HTMLDomParser::str_get_html($html)->find('a[itemprop="url"]')[2]->plaintext);
            $params['chapter_content'] = HTMLDomParser::str_get_html($html)->find('#chapter-c')[0]->innertext;
            $params['chapter_seo_title'] =  $params['chapter_title'];
            $params['chapter_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
            $params['chapter_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
            $params['chapter_order'] = $order;

            $params['created_by_user'] =  1;
            $params['updated_by_user'] =  1;
            return $params;
        }

        return false;
    }
    public function listChapter($story_id, $url)
    {
        $html = $this->curl($url);
        $params = array();
        if ($html) {
            $chapters = HTMLDomParser::str_get_html($html)->find('.list-chapter li a');
            $order = 1;
            foreach ($chapters as $chapter) {
                if ($chapter->href) {
                    $data = $this->fillChapter($order, $chapter->href);
                    if ($data) {
                        $data['story_id'] = $story_id;
                        $params = array('story_id' => $story_id, 'chapter_slug' => $data['chapter_slug']);
                        $count = ChapterService::totalRows($params);
                        if ($count < 1) {
                            ChapterService::insert($data);
                        }
                    }
                }
                $order++;
            }

            $pagination = isset(HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href) ? HTMLDomParser::str_get_html($html)->find('.pagination-sm .active+li a')[0]->href : false;
            if ($pagination) {
                self::listChapter($story_id, $pagination);
            }
            return true;
        }
        return false;
    }

    public function curl($url)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true); // some output will go to stderr / error_log
        //curl_setopt($ch, CURLOPT_REFERER, 'https://gbanime.com/');
        //curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    function grab_image($url, $saveto)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }
}
