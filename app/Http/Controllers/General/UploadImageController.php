<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helpers\UploadImage;

class UploadImageController extends Controller
{
    public function __construct(Request $request,UploadImage $uploadImage)
    {
        $this->request = $request;
        $this->uploadImage = $uploadImage;
    }

    /**
    * ======================
    * Method:: login
    * ======================
    */

    public function uploadImage()
    {
        $image = $this->request->file('file');
        
        $result = $this->uploadImage->uploadAvatar($image, 'news/images/');
        if($result['success']) {
            return response()->json($result);
        }
        return ;
        
    }

    
}
