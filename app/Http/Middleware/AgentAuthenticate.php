<?php

namespace App\Http\Middleware;

use App\Services\Auth\AgentAuthService;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Log;

class AgentAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        Log::info('Log request', ['uri' => $request->path(), 'params' => $request->all()]);
        if (!AgentAuthService::checkLogin()) {
            return redirect('login');
        }

        return $next($request);
    }
}
